# Web-based Planrep
<p align="justify">The Web based Planning and Reporting System (PlanRep4) is a system designed to assist local authorities in planning, budgeting, projecting revenue from all sources, and tracking funds received, physical implementation and expenditure.</p>

<p align="justify"> As always has been a <em>Technical Implications Conundrum</em>; Taking in mind the changes in policies and priorities that always happen in administration .. the new redesigned planrep has embarked on threee important parameters that are .... </p>
 
 <ul>
 	<li>Scalability,</li>
 	<li>Flexibility,</li> 
 	<li>and Configurable.</li>
 </ul>
This is very helpfully as changes will be accomodated smoothly without undergoing major reforms (coding) or revamp of the sysem architecture and structural design <br>

## Scalability
 <p align="justify"> The newly redeisnged planrep has been designed in order to have the capability to handle a growing amount of work and  accommodate the growth/changes of different entities such as <em>accomodation of different sectors, fcilities planning etc</em> . In the newly redesigned planrep the scalability factors has been prioritised in order to keep pace with changes in both policies and guidelines that are/will being/be made by decision makers from time to time.</p>

<p align="justify">Different level of scalability has been implemented in the newly administrative planrep. Some of the features that has been taken care for scalability are: <em>Database, Administrative, functional etc</em></p>
 
 <dl>
 	<dt><strong>Eg. Administrative scalability:</strong></dt>
 	<dd align="justify" >The newly redesigned system has been designed to enable it to accomodate changes in administrative hierarchies such as increasing/deceasing/spliting number of organizations (regions, districts/LGAs, wards, villages etc). The changes in administrative hierarchies/rankings also automatically triggers the changes in <em><strong>users</em></strong> that will access the system. The newl designed planrep scalability enables handling of users according to administrative changes</dd>
 </dl>
	
## Flexibilty

## Configurable

- Easy in maintenance and services <br>
- Centralized system that will iclude all sectors <br>
- Unison plans implementations and processes such as date of opening plannning cycle, closing, submission, approval, phyisical implementations, follow-up, evidence based execution as LGA/implementers will have to provide evidence on all proceedings of how the plans are being implemented etc <br>
 - Changes such as scrutinazation <br>
 - CCHP Assessment <br>
 - Service Output that will affect CoA <br>
 - User Access, Role based, Security <br>
 - Flexible and Scalable Admin Hierarchies from Ministry Level to Lower Level <br>
 - Facility Planning <br>
 - Intergration with other systems
 - Versioning
