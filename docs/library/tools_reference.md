# Softwares and Tools
<p align="justify">The following are the references for the tools and softwares that has been used by the planrep developers
	<ol>
		<li><strong>Database(RDBMS)</strong>: <code><u><a href="https://www.postgresql.org/" title="PostgreSQL" target="_blank">PostgreSQL</a></u></code></li>
		<li><strong>Front-end Desing</strong>:
			<ul style="list-style: square">
				<li><code><u><a href="https://angularjs.org/" title="AngularJS" target="_blank">AngularJS</a></u></code>: This is a JavaScript-based open-source <em>front-end</em> web application framework.</li>
				<li><code><u><a href="http://sass-lang.com/" title="SASS" target="_blank">SASS (syntactically awesome stylesheets)</a></u></code>:  is a scripting language that is interpreted or compiled into Cascading Style Sheets (CSS)</li>
				<li>Other Softwares: <code><u>HTML</u></code>, <code><u>CSS</u></code>, <code><u><a href="https://www.javascript.com/" title="JavaScript" target="_blank">JavaScript</a></u></code></li>
			</ul>
		</li>
		<li><strong>Back-end Design</strong>: <code><u><a href="https://laravel.com/" title="Laravel" target="_blank">Laravel Framework</a></u>, <u><a href="https://www.javascript.com/" title="JavaScript" target="_blank">JavaScript</a></u></code></li>
		<li><strong>Report Design</strong>: <code><u><a href="https://community.jaspersoft.com/project/jaspersoft-studio" title="TIBCO Jaspersoft Studio" target="_blank">Jaspersoft</a></u></code></li>
		<li><strong>Syste Integration</strong>: <code><u><a href="https://www.rabbitmq.com/" title="rabbitmq" target="_blank">RabbitMQ Mesage Broker</a></u></code></li>
	</ol>
</p>
