#GFS Codes
<strong>GFS</strong>: <i><code>Government Financial Statistics Codes</code></i>
<p align="justify">The GFS codes has been classified into different classifications. The following appendix provides all of the classification codes used in the GFS system. </p>

![GFSCodes](/img/GFSCodes.png)

<table  style="text-align: justify; border: 1px solid;">
	<caption style="background-color: lightgrey; ">GFS Codes</caption>
	<tr>
		<td>
			<ol>
				<li>Classification codes are used in the GFS system to identify types of transactions, other economic flows, and stocks of assets and liabilities. This appendix presents in one place all of the codes that were presented in Chapters 5 through 10. The overall organization of the codes is displayed in Figure A1</li>
				<li>Codes beginning with 1 refer to revenue; codes beginning with 2 refer to expense; and codes beginning with 3 refer to transactions in nonfinancial assets, financial assets, and liabilities. For financial assets and liabilities, the code 3 signifies that they have been classified by financial instrument. </li>
				<li>The first digit of the classification code for an other economic flow is always 4 or 5. Codes beginning with 4 refer to holding gains and codes beginning with 5 refer to other changes in the volume of assets and liabilities. The first digit of the classification code for a stock of a type of asset or liability is always 6.</li>
			</ol>
		</td>
		<td>
			<ol start="4">
				<li>Transactions in assets and liabilities, other economic flows, and stocks of assets and liabilities all refer to types of assets and liabilities. Hence, the second and subsequent digits of each code are identical for each type of asset or liability. That is, 311 refers to transactions in fixed assets, 411 to holding gains in fixed assets, 511 to other changes in the volume of fixed assets, and 611 to the stock of fixed assets.</li>
				<li>Expense transactions and transactions in nonfinancial assets can also be classified using the Classification of Functions of Government (COFOG) as described in Chapter 6. All COFOG classification codes begin with 7. Transactions in financial assets and liabilities can be classified according to the sector of the other party to the financial instrument as well as according to the type of financial instrument. When classified by sector, the classification codes for these transactions begin with 8. </li>
				<li>In practical applications, it may be possible and desirable to use more detailed classifications. Such an expansion can be accomplished by adding another digit to any given classification code. For example, the classification code for the stock of transport equipment is 61121. If types of transport equipment were to be classified separately, the codes 611211, 611212, and so forth would be used.</li>
			</ol>
		</td>
	</tr>
</table>
<p>Click on <a href="http://localhost:8000/tools/tools_vi_img/" title="Video Testing" target="">this link</a> to see the diagram and video for more illustration</p>