# <a  name="pe_management" style="color: unset;">Personal Emoluments (PE)
<p align="justify">This is considered to be a special kind of module in the PlanRep design due to its speciality and difference in process as compared to the Development and Other Charges budget classes. This is the module which deals with handling Personal Emolument (PE) budgets. It deals with the tables required for the definition of budget submission forms and their columns as well as the tables required to store the values of the forms. </p>

<p align="center">PE Schema diagram</p>
![pe_schema](../img/budgeting/pe_budgeting/pe_schema.png)

<p align="justify">This sub-module consists of the following tables</p>
<ol>
	<li><a href="#budget_submission_forms" title="budget_submission_forms">budget_submission_forms</a></li>
	<li><a href="#budget_submission_sub_forms" title="budget_submission_sub_forms">budget_submission_sub_forms</a></li>
	<li><a href="#budget_submission_definitions" title="budget_submission_definitions">budget_submission_definitions</a></li>
	<li><a href="#budget_submission_select_option" title="budget_submission_select_option">budget_submission_select_option</a></li>
	<li><a href="#budget_submission_lines" title="budget_submission_lines">budget_submission_lines</a></li>
	<li><a href="#budget_submission_line_values" title="budget_submission_line_values">budget_submission_line_values</a></li>
	<li><a href="#pe_submission_forms_reports" title="pe_submission_forms_reports">pe_submission_forms_reports</a></li>
</ol>

## <a name="budget_submission_forms"></a><u>budget_submission_forms</u>
<p align="justify">This table stores the information of the budget submission form. The table has the following columns.</p>

![budget_submission_forms](../img/budgeting/pe_budgeting/budget_submission_forms.png)

<a href="#pe_management" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#budget_submission_sub_forms" title="Next Table">Next</a>

## <a name="budget_submission_sub_forms"></a><u>budget_submission_sub_forms</u>
<p align="justify">This table stores the information regarding to the subforms of a budget submission form</p>

![budget_submission_sub_forms](../img/budgeting/pe_budgeting/budget_submission_sub_forms.png)

<a href="#pe_management" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#budget_submission_definitions" title="Go Top">Next</a>

## <a name="budget_submission_definitions"></a><u>budget_submission_definitions</u>
<p align="justify">This table stores all the columns which are required in budget submission forms.</p>

![budget_submission_definitions](../img/budgeting/pe_budgeting/budget_submission_definitions.png)

<a href="#pe_management" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#budget_submission_select_option" title="Go Top">Next</a>

## <a name="budget_submission_select_option"></a><u>budget_submission_select_option</u>
<p align="justify">This table stores the information regarding to the select options which are used in budget submission forms. These options are the ones which appear on the drop down lists/menus of the forms.</p>

![budget_submission_select_option](../img/budgeting/pe_budgeting/budget_submission_select_option.png)

<a href="#pe_management" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#budget_submission_lines" title="Go Top">Next</a>

## <a name="budget_submission_lines"></a><u>budget_submission_lines</u>
<p align="justify">This table is used to store the records which deal with the definition of the rows of the tables of budget submission forms.</p>

![budget_submission_lines](../img/budgeting/pe_budgeting/budget_submission_lines.png)

<a href="#pe_management" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#budget_submission_line_values" title="Go Top">Next</a>

## <a name="budget_submission_line_values"></a><u>budget_submission_line_values</u>
<p align="justify">This table is the one which is used to bind the relationship between the budget submission forms, budget submission subforms, budget submission options to be selected and the values to be stored. It is the actual table which stores the values of the personal emolument budgets.</p>

![budget_submission_line_values](../img/budgeting/pe_budgeting/budget_submission_line_values.png)

<a href="#pe_management" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#pe_submission_forms_reports" title="Go Top">Next</a>

## <a name="pe_submission_forms_reports"></a><u>pe_submission_forms_reports</u>
<p align="justify">This table store information for the pe reports. It helps in setting the pe reports on how they will appear when printed out</p>

![pe_submission_forms_reports](../img/budgeting/pe_budgeting/pe_submission_forms_reports.png)

<a href="#pe_management" title="Go Top">Back Top</a>