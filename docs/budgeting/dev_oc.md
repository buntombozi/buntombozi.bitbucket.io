# <a  name="dev_oc" style="color: unset;">Development and Other Charges Modules of Budgeting
<p align="justify">These sub-modules of budgeting follow a process which is more or likely the same. For this reason, these modules share the same tables</p>

<p align="justify">This development and other charges module has the following tables</p>
<ol>
	<li><a href="#activity_facility_fund_source_input_breakdowns" title="activity_facility_fund_source_input_breakdowns">activity_facility_fund_source_input_breakdowns</a></li>
	<li><a href="#admin_hierarchy_ceilings" title="admin_hierarchy_ceilings">admin_hierarchy_ceilings</a></li>
	<li><a href="#admin_hierarchy_ceiling_periods" title="admin_hierarchy_ceiling_periods">admin_hierarchy_ceiling_periods</a></li>
	<li><a href="#bdc_expenditure_centre_values" title="bdc_expenditure_centre_values">bdc_expenditure_centre_values</a></li>
</ol>

## <a name="activity_facility_fund_source_input_breakdowns"></a><u>activity_facility_fund_source_input_breakdowns</u>
<p align="justify">This table stores all the  information regarding to the breakdowns of the inputs of activities</p>

![activity_facility_fund_source_input_breakdowns](../img/budgeting/dev_oc/activity_facility_fund_source_input_breakdowns.png)

<a href="#dev_oc" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#admin_hierarchy_ceilings" title="Next Table">Next</a>

## <a name="admin_hierarchy_ceilings"></a><u>admin_hierarchy_ceilings</u>
<p align="justify">This table stores the ceilings of all the administrative levels.</p>

![admin_hierarchy_ceilings](../img/budgeting/dev_oc/admin_hierarchy_ceilings.png)

<a href="#dev_oc" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#admin_hierarchy_ceiling_periods" title="Go Top">Next</a>

## <a name="admin_hierarchy_ceiling_periods"></a><u>admin_hierarchy_ceiling_periods</u>
<p align="justify">This tables stores the information regarding to the ceilings of the administrative levels of PlanRep for different  quarters/periods</p>

![admin_hierarchy_ceiling_periods_snap](../img/budgeting/dev_oc/admin_hierarchy_ceiling_periods_snap.png)

<a href="#dev_oc" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#bdc_expenditure_centre_values" title="Go Top">Next</a>

## <a name="bdc_expenditure_centre_values"></a><u>bdc_expenditure_centre_values</u>
<p align="justify">This table stores information regarding to the values of different expenditure centres of PlanRep. Expenditure centres are used to define the nature of expenses incurred for an activity. The are used as a model to encapsulate and aggregate the amounts which are spent for items such as medicines, training, construction, etc. </p>

![bdc_expenditure_centre_values](../img/budgeting/dev_oc/bdc_expenditure_centre_values.png)

<a href="#dev_oc" title="Go Top">Back Top</a>