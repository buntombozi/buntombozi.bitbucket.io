# <a  name="scrutinisation_module" style="color: unset;">SCRUTINIZATION
## Purpose of the Scrutinization
<p align="justify"><strong>Scrutinization</strong> is required to check whether the activities are appropriately <em>costed, ceilings</em> have been adhered to, and proper <em>inputs</em> provided. 
Scrutinization starts after the Budget Officer responsible for <em>cost centers</em> have completed the activity costing. </p>

## <code>Scrutinisation Schema</code>
![scrutinisation](../img/general_schema/scrutinisation.png)

## Scrutinisation Tables
<p align="justify">The scrutinisation module has the following tables</p>

<ol>
	<li><a href="#scrutinizations" title="scrutinizations">scrutinizations</a></li>
	<li><a href="#scrutinization_comments" title="scrutinization_comments">scrutinization_comments</a></li>
	<li><a href="#scrutinization_rounds" title="scrutinization_rounds">scrutinization_rounds</a></li>
	<li><a href="#decision_level_admin_hierarchies" title="decision_level_admin_hierarchies">decision_level_admin_hierarchies</a></li>
	<li><a href="#decision_level_next_decisions" title="decision_level_next_decisions">decision_level_next_decisions</a></li>
	<li><a href="#mtef_comments" title="mtef_comments">mtef_comments</a></li>
	<li><a href="#mtef_section_comments" title="mtef_section_comments">mtef_section_comments</a></li>
	<li><a href="#mtef_section_item_comments" title="mtef_section_item_comments">mtef_section_item_comments</a></li>
</ol>

## <a name="scrutinizations"></a><u>scrutinizations</u>
<p align="justify">This table store the flow of information as used in scrutinization. The scrutinization process follows the planning and budgeting (costing) modules. It checks to how conformity of policies, guidelines, national targets etc have been addressed during the planning and costing processes. The scrutinization table also fetches and maps information from several tables such as <code>admin_hierarchies, sections, decision_level, scrutinization_rounds, users </code> etc.</p>

 ![scrutinizations](../img/scrutinisation/scrutinizations.png)

<a href="#scrutinisation_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#scrutinization_comments" title="Next Table">Next</a>

## <a name="scrutinization_comments"></a><u>scrutinization_comments</u>
<p align="justify">This table store comments that are tied to a certain item/budget that is being scrutinized</p>

![scrutinization_comments](../img/scrutinisation/scrutinization_comments.png)

<a href="#scrutinisation_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#scrutinization_rounds" title="Go Top">Next</a>

## <a name="scrutinization_rounds"></a><u>scrutinization_rounds</u>
<p align="justify">This table store number of time (quantity ) an item budget has undergone scrutinization.</p>

![scrutinization_rounds](../img/scrutinisation/scrutinization_rounds.png)

<a href="#scrutinisation_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#decision_level_admin_hierarchies" title="Go Top">Next</a>

## <a name="decision_level_admin_hierarchies"></a><u>decision_level_admin_hierarchies</u>
<p align="justify">This table store mapped information of admin hierarchies and decision levels a budget will pass through scrutinization. It hold mapped information from the <code>decison_levels</code> and <code>admin_hierarchies</code> tables.</p>

![decision_level_admin_hierarchies](../img/scrutinisation/decision_level_admin_hierarchies.png)

<a href="#scrutinisation_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#decision_level_next_decisions" title="Go Top">Next</a>

## <a name="decision_level_next_decisions"></a><u>decision_level_next_decisions</u>
<p align="justify">This table store information for the chronological order of decisions during scrutinization. The scrutinization decision flow from lower levels to higher levels and vice versa. Eg <code><em>Cost centre > department > Council > Region > PO-RALG</em></code> etc </p>

![decision_level_next_decisions](../img/scrutinisation/decision_level_next_decisions.png)

<a href="#scrutinisation_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#mtef_comments" title="Go Top">Next</a>

## <a name="mtef_comments"></a><u>mtef_comments</u>
<p align="justify"><code>MTEF_COMMENTS TABLE HERE ...............</code></p>

![mtef_comments](../img/scrutinisation/mtef_comments_snap.png)

<a href="#scrutinisation_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#mtef_section_comments" title="Go Top">Next</a>

## <a name="mtef_section_comments"></a><u>mtef_section_comments</u>
<p align="justify"><code>MTEF_SECTION_COMMENTS TABLE DESCRIPTION WILL APPEAR HERE .............</code></p>

![mtef_section_comments](../img/scrutinisation/mtef_section_comments_snap.png)

<a href="#scrutinisation_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#mtef_section_item_comments" title="Go Top">Next</a>

## <a name="mtef_section_item_comments"></a><u>mtef_section_item_comments</u>
<p align="justify"><code>MTEF_SECTION_ITEM_COMMENTS TABLE DESCRIPTIONS WILL APPEAR HERE .............</code></p>

![mtef_section_item_comments](../img/scrutinisation/mtef_section_item_comments_snap.png)

<a href="#scrutinisation_module" title="Go Top">Back Top</a>

