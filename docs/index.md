# About PlanRep
</p>PlanRep was developed to assist Local Government Authorities in planning, budgeting, projecting revenue, tracking funds received, actual implementation and financial and non-financial reporting. </p>
PlanRep acts as the main information source for integration of community plans and the LGA strategic plan for a specific financial year. 
Within PlanRep, councils are able define their Medium Terms Expenditure Framework (MTEF) in relation to their Strategic Plans (SP) which includes objectives, targets, and activities. 
PlanRep is also used in preparing the councils development and other charges line items in the budget by identifing funding sources and amounts for each activity. 
It also contains tools required for preparing the Comprehensive Council Health Plan (CCHP). 

## Advantages of PlanRep
*   Timely, accurate, and consistent data for management and budget decision-making; 
* 	Integration of budget and budget execution data, allowing greater financial control and reducing opportunities for discretion in the use of public funds;
* 	Producing MTEF format which includes Objectives, Targets and Activities which are the requirements for LGAs planning for the sake of consistency with MoFP and PO-RALG reporting;
* 	Assuring accuracy of numerical calculations of allocation ceilings for both service provider and cost centers also types of expenditure;
* 	Automatic production of most reports specified in the LGAs Planning and Budget Guidelines; 
* 	Automatic calculation of budget allocation to the activities and Plan of Action which is created instantaneously and it is also completely consistent with all data entered;
* 	Improving business processes and the planning, budgeting, reporting cycle including increasing participation in the planning process and strengthening planning structure and content for programs and services
* 	Minimizing the reporting burden; Facilitating financial statement preparation (Budget versus Actual Expenditure);
* 	Consistency within all reporting in standard formats;
* 	Easing of aggregation at regional and national level of all district-level data, allowing cross-district and cross-region analysis; and
* 	Potential for integrated reporting on physical implementation and expenditures