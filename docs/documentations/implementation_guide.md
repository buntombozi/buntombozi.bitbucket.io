# Implementation Guide
## About this guide
<p align="justify">The Planrep documentation is a collective effort and has been developed by the development team and users. While the guide strives to be complete, there may be certain functionalities which have been omitted or which have yet to be documented. 
This section explains some of the conventions which are used throughout the document.Planrep is a browser-based application. In many cases, screenshots have been included for enhanced clarity. 
Shortcuts to various functionalities are displayed such as <strong>Data element</strong> > <strong>Data element group</strong>. The <em>">"</em> symbol indicates that you should click Data element and then click Data element group in the user interface.
Different styles of text have been used to highlight important parts of the text or particular types of text, such as source code. Each of the conventions used in the document are explained below.</p>
<p><a href="https://docs.dhis2.org/master/en/implementer/html/dhis2_implementation_guide_full.html" title="Implementation Guide - DHIS2" target="_blank">Implementation Guide - DHIS2</a></p>