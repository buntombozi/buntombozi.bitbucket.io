# Developer Guide
<p><a href="https://docs.dhis2.org/master/en/developer/html/dhis2_developer_manual_full.html" title="Developer Guide" target="_blank">Technical Documentation - Developer Guide</a></p>
<p><a href="https://wiki.ihris.org/wiki/Technical_Documentation" title="Technical Documentation - iHRIS" target="_blank">Technical Documentation - iHRIS</a></p>
<p><a href="https://wiki.ihris.org/wiki/Interoperability" title="Technical Documentation - iHRIS" target="_blank">Technical Documentation - iHRIS | Interoperability</a></p>
<p><a href="https://wiki.ihris.org/wiki/Introduction_and_Overview" title="Technical Documentation - iHRIS Technical Overview" target="_blank">Technical Documentation - iHRIS | Modules</a></p>
<p><a href="https://wiki.ihris.org/wiki/Data_Dictionary_-_iHRIS_4.1" title="Technical Documentation - iHRIS Data Dictionary" target="_blank">Technical Documentation - iHRIS Data Dictionary</a></p>