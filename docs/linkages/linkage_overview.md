# System Integration
## Core National Systems
<p><strong>PlanRep</strong> will be linked with other core National systems in order to enable smooth execution of operations through synchronization and communications between systems.</p>
The following is the list of core national systesm that will be linked with planrep
<ol>
	<li><a href="#" title="Epicor" target="_blank">EPICOR</a></li>
	<li><a href="http://196.192.72.106/#!/login" title="FFARS" target="_blank">FFARS</a></li>
	<li><a href="#" title="LGRCIS" target="_blank">LGRCIS</a></li>
	<li><a href="#" title="DHIS2" target="_blank">DHIS2</a></li>
	<li><a href="#" title="LAWSON" target="_blank">LAWSON | HCMIS</a></li>
	<li><a href="#" title="GoT-HOMIS" target="_blank">GoT-HOMIS</a></li>
</ol>

<p align="justify">The following image shows the rough work on system linkages</p>
![system_integration](/img/system_integration.png)

### <b>Advantages of System Linkages</b>
The following are some of the most important results in linking systems together
<ul>
	<li>Improve efficiency and/or reduce the risk of error as a result of duplication of efforts.</li>
	<li>Resource Sharing</li>
</ul>
