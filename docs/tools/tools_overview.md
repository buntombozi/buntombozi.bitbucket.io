# Overview of Planrep Design Pattern and Approach
<p align="justify">To start with, planrep has been developed from open-source softwares and tools</p>
<p align="justify"> Planrep makes use of multiple tables in a relational database (PostgreSQL) to store its data in. Database Structure describes the tables used by planrep, in particular how the tables has been arranged in modules basis and hence different schemas. The planrep system design approach follows the <em><strong>Model-View-Controller (MVC)</strong></em> design pattern.</p>

<p align="justify">This documentation describes the Planrep Module Structures (database schemas). In planrep, a module is a collection of various types of <em>"code"</em> by the features that they provide to the system. Modules serve as the building blocks of planrep and are grouped according to the functionality they provide and serve. For the this documentation purpose we will look on the database schemas that made up a module</p>

## Why Modules design (MVC)
<p align="justify">Planrep has adopted the MVC design pattern and implemented a module design approach. The planrep MVC pattern consists of front-end and a back-end approach (connected via a service layer) for performance optimisation.</p>

![front_back_end_mvc](../img/front_back_end_mvc.png)

<p align="justify">Some of the advantages of the module approach in designing planrep are:-</p>
<ul align="justify">
	<li>The main reason to use a module system is to increase code manageability. The various components of the planrep system (Planning, Budgeting, Assessment, Scrutinization) are used in a variety of settings each requiring their own customizations.</li>
	<li>Each of the planrep components (Planning, Budgeting, Assessment) share some common functionality and the module system ensures proper code re-use.</li>
	<li>The module system lets many customizations to be encapsulated without having to change the underlying code base.</li>
	<li>As modules are organized by the functionality that they provide to the system, a developer can quickly find the relevant code to change by looking at the relevant modules.</li>
	<li>Customizations to the software encapsulated in a module can be easily shared among developers.</li>
</ul>




