# System Installation
## Development Environment
Planrep system is designed in PHP, HTML, Javascript, Angular JS, Jaspersoft. In development enviroment a developer will need to have important tools in 

### Hardware/Computer Specifications
* `At least 4GB RAM ` - For better perfomance.
* `500GB HDD` - External HDD.

### Accessing Database
Planrep uses PostgreSQL as the RDBMS. Hence you need to have PostgreSQL installed to your computer to run the database. Apart from the PostreSQL you can use some tools to access the database such as pgAdmin4++, SQL Manager for PostgreSQL (Note: Some of the softwares are commercially licenced). <br />

### RDBMS
* `PostgreSQL 6+` - The planrep system database has been designed and implemented through PostreSQL | read mored about <a href="https://www.postgresql.org/" title="Official Page of PostgreSQL" target="_blank">PostgreSQL</a>

### Softwares and Tools
* `Web Servers` - Apache [XAMP/WAMP] .. for windows we recommend XAMP.
* `Angular JS` - for front end designing.
* `Laravel 5.4++ PHP Framework` - More about the <a href="https://laravel.com/" title="Official Laravel" target="_blank">Laravel PHP framework.</a>
* `Jaspersoft` - for report designing.

<p align="justify" ><strong>NB:&nbsp;</strong>You can read more on the above tools through their official documentation for more fine tuning and better performances settings</p>