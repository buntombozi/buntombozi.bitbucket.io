# Planrep Development Tools
<p align="justify">PlanRep is a web-based application developed using an open source PHP Platform application called <strong>Laravel</strong> Framework, hence the redesigned planrep has been developed mainly by PHP. The Laravel Framework implements the MVC design pattern that has been fully implemented during planrep redesigning. For performance optimasation the planrep MVC design has also employed a <em><strong>front-end, back-end</strong></em> approach.</p> 
<p> The following is the list of tools that has been used for planrep system development
	<ul>
		<li><u>Database(RDBMS)</u> : <code>PostgreSQL</code></li>
		<li><u>Front-end Desing</u> : <code>HTML, CSS, JavaScript, SASS, AngularJS</code></li>
		<li><u>Back-end Design</u> : <code>PHP (Laravel Framework), JavaScript</code></li>
		<li><u>Report Design</u> : <code>Jaspersoft</code></li>
	</ul>
</p>
<p align="justify"><strong><u>Note:</u></strong>&nbsp;The planrep system runs in both windows and Linux OS</p>