# <a name="facility_plans" style="color: unset;">Facility Planning Module
<p align="justify">The facility planning module is a mini-module from a planning module that allows facilities plans and programs to be tracked directs from the facility. The module is part of the redesigned planrep that will enable facilities to have their plans incoporated into LGAs plans.</p>

## <code>Facility Plans Schema Diagram</code>

![facility_planning_schema](../img/general_schema/facility_planning_schema.png)































<a href="#facility_plans">Back Top</a>
