# Comprehensive Council Plans Module
<p align="justify">This module has been designed to accommodate comprehensive plans for a specific sector. To start with available plans as shown in the requirements document, the module contains two plans; Comprehensive plans for health sector (CCHP) and Health Facility Plans. More plans can be added when the new requirement arise.</p>

## ER Diagram for CCP Module
The ER diagram  shows how tables that forms and complete the CCHP have been organised and how they relate, depend each other in accomplishing the tasks under the CCHP Module. 

Note: The tables contents has been minimised in order to fit the page.

![CCHP Module ER Diagram](../img/cchp/cchp_tables_module.jpg)

