# <a name="home" style="color: unset;"> CCHP Module Tables
<p>The CCP/CCHP Module has the following tables</p>
<ol>
	<li><a href="#casp" title="cas_plans">cas_plans</a></li>
	<li><a href="#casptc" title="cas_plan_table_columns">cas_plan_table_columns</a></li>
	<li><a href="#caspc" title="cas_plan_contents">cas_plan_contents</a></li>
	<li><a href="#caspt" title="cas_plan_tables">cas_plan_tables</a></li>
	<li><a href="#caspti">cas_plan_table_items</a></li>
	<li><a href="#casptic" title="cas_plan_table_item_constants">cas_plan_table_item_constants</a></li>
	<li><a href="#casptiv" title="cas_plan_table_item_values">cas_plan_table_item_values</a></li>
	<li><a href="#casgc" title="cas_group_columns">cas_group_columns</a></li>
	<li><a href="#casgt" title="cas_group_types">cas_group_types</a></li>
	<li><a href="#casptd" title="cas_plan_table_details">cas_plan_table_details</a></li>
</ol>
<br>
# CCHP Tables
## <a name="casp"></a><u>cas_plans</u>
<p align="justify">This table stores comprehensive plans for a specific sector. All plans are registered during system setup in the setup module including CCHP, Facility Plans and etc.</p>

![cas_plans](../img/cchp/cas_plans.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casptc"></a><u>cas_plan_table_columns</u>
This table defines the columns of the cas_plan_table

![cas_plan_table_columns](../img/cchp/cas_plan_table_columns.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="caspc"></a><u>cas_plan_contents</u>
<p align="justify">The table contains contents structure of the comprehensive plans registered in cas_plans table. Every comprehensive plans should have its own contents structure. The contents of a comprehensive plan is defined in the setup module during system setup.</p>

![cas_plan_contents](../img/cchp/cas_plan_contents.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="caspt"></a><u>cas_plan_tables</u>
<p align="justify">This table register tables which links to the content item of the comprehensive plans. One content item can be related to more than one tables. In order for the content item to be accessed by the user when clicked, it should be linked to one or may tables. Table gives three options: to design input table with rows and columns, a link to a  report or provide a window for user to upload file.</p>

![cas_plan_tables](../img/cchp/cas_plan_tables.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="caspti"></a><u>cas_plan_table_items</u>
<p align="justify">This table stores table items/ rows of the table. This field is optional due to the fact that some table does not have table rows defined at the beginning. </p>

![cas_plan_table_items](../img/cchp/cas_plan_table_items.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casptic"></a><u>cas_plan_table_item_constants</u>
<p align="justify">This table stores global items of the table on specific admin hierarchy level assigned. For example  some of the data are entered at national or regional level and used at lower levels. This table is designed to avoid multiple data entry at lower levels.</p>

![cas_plan_table_item_constants](../img/cchp/cas_plan_table_item_constants.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casptiv"></a><u>cas_plan_table_item_values</u>
<p align="justify">This table stores data resulted from the table defined by the user during configuration.  It holds data for every financial year, in short it keeps the history of the table data.</p>

![cas_plan_table_item_values](../img/cchp/cas_plan_table_item_values.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casgc"></a><u>cas_group_columns</u>
<p align="justify">This table stores group columns to simplify table configuration and also to make sure columns of the same nature originated from the same group. This  will enable further analysis  and comparison of different table which holds values of the same nature. For example gender group holds Male &amp; Female.</p>

![cas_group_columns](../img/cchp/cas_group_columns.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casgt"></a><u>cas_group_types</u>
<p align="justify">This table stores group of cas_plan_group_columns. For example group type  gender, age and etc</p>

![cas_group_types](../img/cchp/cas_group_types.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casptd"></a><u>cas_plan_table_details</u>
<p align="justify">This table stores table  comments and keys for every financial year. This table also stores the link to documents uploaded to comprehensive plans of the specific financial year.</p>

![cas_plan_table_details](../img/cchp/cas_plan_table_details.png)

<a href="#home" title="Go Top">Back Top</a>