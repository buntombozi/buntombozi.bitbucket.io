# <a  name="notifications" style="color: unset;">Notifications
<p align="justify">This module is used to deliver instant message to user on every important step that he has to take or he has to know. An event like submission of budget or returning of budget to a lower decision level will trigger notification and send on screen messages and other form of messages to a list of recipients that are involved.</p>

<p align="justify"> A number of unread notifications for a particular user will be displayed at the top of the screen whenever a user is logged in and new notifications will pop up and add number instantly. A user can also view previous notifications
 </p>
<p align="justify"> The notifications are recorded in a table called <code><strong><a href="#notifications" title="notifications">notifications</a></strong></code>.</p>

## <a name="notifications"></a><u>notifications</u>
<p align="justify">Notifications are saved on a table called notifications before they are published to a redis server and they will stay there for user to access them later on. Marking notifications as <mark>read</mark> is done in this table</p>

![notifications](../img/notifications/notifications.png)