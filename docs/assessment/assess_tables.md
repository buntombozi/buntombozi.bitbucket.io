# <a name="home" style="color: unset;"> Assessment Module Tables

This module has the following tables
<ol>
	<li><a href="#casass_cat" title="cas_assessment_categories">cas_assessment_categories</a></li>
	<li><a href="#casass_catv" title="cas_assessment_category_versions">cas_assessment_category_versions</a></li>
	<li><a href="#casass_catvst" title="cas_assessment_category_version_states">cas_assessment_category_version_states</a></li>
	<li><a href="#casass_copt" title="cas_assessment_criteria_options">cas_assessment_criteria_options</a></li>
	<li><a href="#casass_rest" title="cas_assessment_results">cas_assessment_results</a></li>
	<li><a href="#casass_restdet" title="cas_assessment_result_details">cas_assessment_result_details</a></li>
	<li><a href="#casass_round" title="cas_assessment_rounds">cas_assessment_rounds</a></li>
	<li><a href="#casass_stat" title="cas_assessment_states">cas_assessment_states</a></li>
	<li><a href="#casass_subcopt" title="cas_assessment_sub_criteria_options">cas_assessment_sub_criteria_options</a></li>
	<li><a href="#casass_subcpscor" title="cas_assessment_sub_criteria_possible_scores">cas_assessment_sub_criteria_possible_scores</a></li>
	<li><a href="#casass_subcrepset" title="cas_assessment_sub_criteria_report_sets">cas_assessment_sub_criteria_report_sets</a></li>
	<li><a href="#casass_redetrep" title="cas_assessment_result_detail_replies">cas_assessment_result_detail_replies</a></li>
	<li><a href="#casass_resrep" title="cas_assessment_result_replies">cas_assessment_result_replies</a></li>
</ol>

## <a name="casass_cat"></a><u>cas_assessment_categories</u>
<p align="justify">This table stores Assessment Categories, the categories specifies what is going to be assessed and when.  An  Example of Assessment category is Annual CCHP Assessment. This table is available during settings and It is used during assessment. A user will choose the assessment category before starting assessment.</p>

![cas_assessment_categories](../img/assessment/cas_assessment_categories.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_catv"></a><u>cas_assessment_category_versions</u>
<p align="justify">Each time the assessment category has new assessment criteria sets we produce a new category version. These versions are store in cas_assessment_category_versions and the assessment criteria are linked to this table.</p>

![cas_assessment_category_versions](../img/assessment/cas_assessment_category_versions.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_catvst"></a><u>cas_assessment_category_version_states</u>
<p align="justify">This table store assessment states, it can be <em><strong>“not assessed”</strong></em>, <em><strong>“Recommended”</strong></em>, <em><strong>“Not Recommended”</strong></em></p>

![cas_assessment_category_version_states](../img/assessment/cas_assessment_category_version_states.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_copt"></a><u>cas_assessment_criteria_options</u>
<p align="justify">Every assessment category version has a set of criteria, we call the criteria options and are stored in this table. These tells us what we need to assess in that assessment, the area to be assessed</p>

![cas_assessment_criteria_options](../img/assessment/cas_assessment_criteria_options.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_rest"></a><u>cas_assessment_results</u>
<p align="justify">A complete assessment is called assessment result, from this table and other related tables you can get everything about an assessment that was perfomed. This tells us The category of assessment, period, assessor, state and scores</p>

![cas_assessment_results](../img/assessment/cas_assessment_results.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_restdet"></a><u>cas_assessment_result_details</u>
<p align="justify">Assessment Results give us a broad picture, a total score is assessment result come from individual scores in the result details. Every sub criteria is assessed marked and comments are given</p>

![cas_assessment_result_details](../img/assessment/cas_assessment_result_details.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_round"></a><u>cas_assessment_rounds</u>
<p align="justify">This table store description for assessment rounds, a round is an occassion of the same assessment category for the same period and the same Admin Hierarchy at the same level of assessment. Each round is done and if it’s not final the required improvements are done  and another round comes.</p>

![cas_assessment_rounds](../img/assessment/cas_assessment_rounds.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_stat"></a><u>cas_assessment_states</u>
<p align="justify">This table store description of assessment states, more information can be seen in the tables that utilizes this table</p>

![cas_assessment_states](../img/assessment/cas_assessment_states.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_subcopt"></a><u>cas_assessment_sub_criteria_options</u>
<p align="justify">This table store sub criteria otions, each assessment criteria has sub criteria, which tells how to assess that criteria.</p>

![cas_assessment_sub_criteria_options](../img/assessment/cas_assessment_sub_criteria_options.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_subcpscor"></a><u>cas_assessment_sub_criteria_possible_scores</u>
<p align="justify">Each sub criteria has it’s own possible score, this table gives description on each score for a corresponding subcriteria. It tells how to score.</p>

![cas_assessment_sub_criteria_possible_scores](../img/assessment/cas_assessment_sub_criteria_possible_scores.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_subcrepset"></a><u>cas_assessment_sub_criteria_report_sets</u>
<p align="justify">This stores reports that are required during assessment, each subcriteria has a set of reports that must be visited before giving score and comments</p>

![cas_assessment_sub_criteria_report_sets](../img/assessment/cas_assessment_sub_criteria_report_sets.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_redetrep"></a><u>cas_assessment_result_detail_replies</u>
<p align="justify">This table store information on replies made by users on a result detail comment. After an assessment is submitted, the responsible users in the next Admin Hierarchy and the submitting Hierarchy can interchange words regarding a certain marked subcriteria through replies.</p>

![cas_assessment_result_detail_replies](../img/assessment/cas_assessment_result_detail_replies.png)

<a href="#home" title="Go Top">Back Top</a>

## <a name="casass_resrep"></a><u>cas_assessment_result_replies</u>
<p align="justify">This table store information on OverallReply on an Assessment. After submitting the assessment, resposible users will go throught the result, reply on comments made by other users or put their own comment as a reply to the comment made during assessment. Finally a user will put a comment at the bottom as a conclusion or overall observation or a reply to the overall coment made during assessment</p>

![cas_assessment_result_replies](../img/assessment/cas_assessment_result_replies.png)

<a href="#home" title="Go Top">Back Top</a>
