# <a  name="planning_module" style="color: unset;">Planning Module Tables

<p align="justify">This module has the following tables</p>
<ol>
	<li><a href="#activities" title="activities">activities</a></li>
	<li><a href="#activity_periods" title="activity_periods">activity_periods</a></li>
	<li><a href="#admin_hierarchy_lev_sec_mappings" title="admin_hierarchy_lev_sec_mappings">admin_hierarchy_lev_sec_mappings</a></li>
	<li><a href="#mtef_annual_targets" title="mtef_annual_targets">mtef_annual_targets</a></li>
	<li><a href="#assets" title="assets">assets</a></li>
	<li><a href="#baseline_statistic_values" title="baseline_statistic_values">baseline_statistic_values</a></li>
	<li><a href="#cas_plan_table_item_admin_hierarchies" title="cas_plan_table_item_admin_hierarchies">cas_plan_table_item_admin_hierarchies</a></li>
	<li><a href="#long_term_targets" title="long_term_targets">long_term_targets</a></li>
	<li><a href="#mtefs" title="mtefs">mtefs</a></li>
	<li><a href="#mtef_sections" title="mtef_sections">mtef_sections</a></li>
	<li><a href="#mtef_sector_problems" title="mtef_sector_problems">mtef_sector_problems</a></li>
	<li><a href="#performance_indicator_baseline_values" title="performance_indicator_baseline_values">performance_indicator_baseline_values</a></li>
	<li><a href="#performance_indicator_financial_year_values" title="performance_indicator_financial_year_values">performance_indicator_financial_year_values</a></li>
	<li><a href="#projections" title="projections">projections</a></li>
	<li><a href="#projection_periods" title="projection_periods">projection_periods</a></li>
	<li><a href="#reference_documents" title="reference_documents">reference_documents</a></li>
	<li><a href="#sector_targets" title="sector_targets">sector_targets</a></li>
	<li><a href="#generic_target_cost_centres" title="generic_target_cost_centres">generic_target_cost_centres</a></li>
	<li><a href="#generic_target_national_references" title="generic_target_national_references">generic_target_national_references</a></li>
	<li><a href="#generic_target_performance_indicators" title="generic_target_performance_indicators">generic_target_performance_indicators</a></li>
	<li><a href="#generic_templates" title="generic_templates">generic_templates</a></li>
	<li><a href="#reference_type_sectors" title="reference_type_sectors">reference_type_sectors</a></li>
	<li><a href="#responsible_persons" title="responsible_persons">responsible_persons</a></li>
	<li><a href="#indicators" title="indicators">indicators</a></li>
	<li><a href="#project_funders" title="project_funders">project_funders</a></li>
</ol>

## <a name="activities"></a><u>activities</u>
<p align="justify">This table store information about activities to be  performed during a financial year.</p>

![activities](../img/planning/activities.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#activity_periods" title="Next Table">Next</a>

## <a name="activity_periods"></a><u>activity_periods</u>
<p align="justify">This is a pivot table between activities and periods. It stores the information about which period(s) is the activity to be performed .This table is in planning module and it is filled with information during activity creation when planning. It maps information from the <code>activities</code> and <code>periods</code> tables</p>

![activity_periods](../img/planning/activity_periods.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#admin_hierarchy_lev_sec_mappings" title="Go Top">Next</a>

## <a name="admin_hierarchy_lev_sec_mappings"></a><u>admin_hierarchy_lev_sec_mappings</u>
<p align="justify">This table store mapping information of which section at which admin hierarchy level can budget, can create target and/or section is used at that admin level</p>

![admin_hierarchy_lev_sec_mappings](../img/planning/admin_hierarchy_lev_sec_mappings.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#mtef_annual_targets" title="Go Top">Next</a>

## <a name="mtef_annual_targets"></a><u>mtef_annual_targets</u>
<p align="justify">This table stores annual targets for a particular targeting section of a particular admin hierarchy in a particular financial year. This table is filled with data when </p>
<ol>
	<li>creating long term target and during financial year initialization  (if system is configured not to use annual targets ) </li>
	<li>When user create annual target (if system is configured to use annual targets)</li>
</ol>

![mtef_annual_targets](../img/planning/mtef_annual_targets.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#assets" title="Go Top">Next</a>

## <a name="assets"></a><u>assets</u>
<p align="justify">This is the table which is used to store the information regarding to the vehicles owned by an LGA. It is used in the comprehensive plans to store records related to vehicles and their use.</p>

![assets](../img/planning/assets.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#baseline_statistic_values" title="Go Top">Next</a>

## <a name="baseline_statistic_values"></a><u>baseline_statistic_values</u>
<p align="justify">This table stores the values of the baseline statistics used for planning and budgeting.</p>

![baseline_statistic_values](../img/planning/baseline_statistic_values.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#cas_plan_table_item_admin_hierarchies" title="Go Top">Next</a>

## <a name="cas_plan_table_item_admin_hierarchies"></a><u>cas_plan_table_item_admin_hierarchies</u>
<p align="justify">This table store information mapped information from <code>admin_hierarchies, cas_plan_tables</code> and <code>cas_plan_table_items </code> tables.</p>

![cas_plan_table_item_admin_hierarchies](../img/planning/cas_plan_table_item_admin_hierarchies.png)

<a href="#planning_module" title="Go Top">Back Top</a>&nbsp; &nbsp; |&nbsp; &nbsp; <a href="#long_term_targets" title="Go Top">Next</a>

## <a name="long_term_targets"></a><u>long_term_targets</u>
<p align="justify">This table stores long term targets that are sometimes referred as national targets. From the long term targets a user can create other targets such as <em>annual targets, generic targets, sector problems </em> etc. </p>

![long_term_targets](../img/planning/long_term_targets.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#mtefs" title="Next Table">Next</a>

## <a name="mtefs"></a><u>mtefs</u>
<p align="justify">The mtef table store information</p>

![mtefs](../img/planning/mtefs.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#mtef_sections" title="Go Top">Next</a>

## <a name="mtef_sections"></a><u>mtef_sections</u>
<p align="justify">This table stores informations ..</p>

![mtef_sections](../img/planning/mtef_sections.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#mtef_sector_problems" title="Go Top">Next</a>

## <a name="mtef_sector_problems"></a><u>mtef_sector_problems</u>
<p align="justify">This table store mapped information form <code>mtefs</code> and <code>sector_problems</code> tables. This information is usefully in specification of sectoral problems as users belongs to a certain sector will only see their sectoral problems (exceptions applies)</p>

![mtef_sector_problems](../img/planning/mtef_sector_problems.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#performance_indicator_baseline_values" title="Go Top">Next</a>

## <a name="performance_indicator_baseline_values"></a><u>performance_indicator_baseline_values</u>
<p align="justify">This table store baseline informations from which a performance indicator will refer.</p>

![performance_indicator_baseline_values](../img/planning/performance_indicator_baseline_values.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#performance_indicator_financial_year_values" title="Go Top">Next</a>

## <a name="performance_indicator_financial_year_values"></a><u>performance_indicator_financial_year_values</u>
<p align="justify">This table store information ....</p>

![performance_indicator_financial_year_values](../img/planning/performance_indicator_financial_year_values.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#projections" title="Go Top">Next</a>

## <a name="projections"></a><u>projections</u>
<p align="justify">This table store projections on funds and revenues that are expected to be affected in the given period (financial year). The informations on this table are mapped from the <code>admin_hierarchies, financial_years</code> and <code>fund_sources </code> tables. </p>

![projections](../img/planning/projections.png)

<a href="#planning_module" title="Go Top">Back Top</a>&nbsp; &nbsp; |&nbsp; &nbsp; <a href="#projection_periods" title="Go Top">Next</a>

## <a name="projection_periods"></a><u>projection_periods</u>
<p align="justify">This table store periods that are used in projections. Fund and revenue flows periodically and hence the projections also follows the periodically trends. Eg, High cultivation season vs low season.</p>

![projection_periods](../img/planning/projection_periods.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#reference_documents" title="Next Table">Next</a>

## <a name="reference_documents"></a><u>reference_documents</u>
<p align="justify">This table store reference documents from which the planning (activities) design and assignments will rely upon.</p>

![reference_documents](../img/planning/reference_documents.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#sector_targets" title="Go Top">Next</a>

## <a name="sector_targets"></a><u>sector_targets</u>
<p align="justify">This table store information about targets that are specific for a given sector. The data in this table is mapped from the <code>sectors</code> and <code>long_term_targets</code> tables.</p>

![sector_targets](../img/planning/sector_targets.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#generic_target_cost_centres" title="Go Top">Next</a>

## <a name="generic_target_cost_centres"></a><u>generic_target_cost_centres</u>
<p align="justify">This table store information for the generic targets created specifically for cost centres. It maps information from generic targets with a sections they belong. ie from the <code>generic_targets</code> and <code>sections table</code></p>

![generic_target_cost_centres](../img/planning/generic_target_cost_centres.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#generic_target_national_references" title="Go Top">Next</a>

## <a name="generic_target_national_references"></a><u>generic_target_national_references</u>
<p align="justify">This table store information for the generic target that are formed during the planning process referring to the national references documents. All generic target must have a trace on the reference they originate from. It actually holds mapped data from <code>generic_targets</code> and <code>reference_docs</code> tables.</p>

![generic_target_national_references](../img/planning/generic_target_national_references.png)

<a href="#planning_module" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#generic_target_performance_indicators" title="Go Top">Next</a>

## <a name="generic_target_performance_indicators"></a><u>generic_target_performance_indicators</u>
<p align="justify">This table store information of a performance indicator that will measure a certain generic target. It holds mapped data from <code>generic_targets</code> and <code>performance_indicators</code> tables</p>

![generic_target_performance_indicators](../img/planning/generic_target_performance_indicators.png)

<a href="#planning_module" title="Go Top">Back Top</a>&nbsp; &nbsp; |&nbsp; &nbsp; <a href="#generic_templates" title="Go Top">Next</a>


## <a name="generic_templates"></a><u>generic_templates</u>
<p align="justify">This table store information for generic templates</p>

![generic_templates](../img/planning/generic_templates.png)

<a href="#planning_module" title="Go Top">Back Top</a>&nbsp; &nbsp; |&nbsp; &nbsp; <a href="#reference_type_sectors" title="Go Top">Next</a>


## <a name="reference_type_sectors"></a><u>reference_type_sectors</u>
<p align="justify">This table store information for references according to sector. It hold mapped informations from the <code>reference_types</code> and  <code>sectors</code> tables</p>

![reference_type_sectors](../img/planning/reference_type_sectors.png)

<a href="#planning_module" title="Go Top">Back Top</a>&nbsp; &nbsp; |&nbsp; &nbsp; <a href="#responsible_persons" title="Go Top">Next</a>


## <a name="responsible_persons"></a><u>responsible_persons</u>
<p align="justify">This table store information of a person that will be responsible to perform a certain activities. The responsible person information are used during planning process where an activity or project must be assigned to an individual. Later the information is used by the scrutinization and  execution modules.</p>

![responsible_persons](../img/planning/responsible_persons.png)

<a href="#planning_module" title="Go Top">Back Top</a>&nbsp; &nbsp; |&nbsp; &nbsp; <a href="#indicators" title="Go Top">Next</a>

## <a name="indicators"></a><u>indicators</u>
<p align="justify">This table store information for indicators . indicators will be used by modules such as execution, assessment</p>

![indicators](../img/planning/indicators.png)

<a href="#planning_module" title="Go Top">Back Top</a>&nbsp; &nbsp; |&nbsp; &nbsp; <a href="#project_funders" title="Go Top">Next</a>

## <a name="project_funders"></a><u>project_funders</u>
<p align="justify">This table store information about funders that fund a particular projects. It is used during planning and budgeting modules as every project must be assigned a funder. It maps informations from the <code>funders</code>  and <code>projects</code> tables.</p>

![project_funders](../img/planning/project_funders.png)

<a href="#planning_module" title="Go Top">Back Top</a>
