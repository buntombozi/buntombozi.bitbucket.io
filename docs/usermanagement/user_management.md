# User Management
<p align="justify">The web-based planrep uses <i>'User management Module'</i> to enact, enforce and enables system administrators to create and manage login credentials for each user. 
Through user management you can also limit and leverage privileges that each user can access and operate. 
Thus, username and password is required to access different applications that are available to the user.</p>

## <code>User Management Schema Diagram</code>
![usermanagement](../img/general_schema/usermanagement.png)

## Controls
<p align="justify">In Planrep the user management has been designed so as to have controls on how users can interact with the system such as  </p>
<ul>
	<li><strong>Horizon:</strong> The applications/screen and the features a user can access within planrep Interface, if applicable </li>
	<li><strong>Access Type:</strong> Hierarchical and on responsibility-wise Eg. planners, assessors, decision makers</li>
	<li><strong>Actions:</strong> read-only, read-writeIf Eg. If a user can create, update information</li>
</ul>

Thus this enable the system to control and leverage access, for example:
<ul style= "list-style-type: square;" >
	<li><em>The applications a user can access, Whether access is read-only, If a user can update information, The features a user can access within planrep Interfaces, if applicable</em></li>
</ul>
<p align="justify"> Also to enhance the performance optimisation and proper resource utilizisation planrep has implemented the <strong>throttle</strong> methodology where it regulates the rate logins among users </p>

## <code>User Access Rights Schema Diagram</code>
![Acess_Rights](../img/general_schema/access_rights_schema.png)

## <strong>Advantages of user management and controls</strong>
<ul>
	<li>It contributing to the overall security of the system</li>
	<li>Makes it easier for  users to access the pages they need</li>
	<li>Simplifies deployment of services within organisation as each individual will have access to the system per his/her scope of work</li>
</ul>

<p align="justify"> For example, health secretaries/health sectpors need access to the parts of the planrep Interface that manages CCHP, while system administrators want to configure the task the users will perform in the planrep system.<p>