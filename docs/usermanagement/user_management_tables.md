# <a name="usermanagement" style="color: unset;">User Management Tables
<p align="justify">The user management modules in general is an authentication feature that provides the planrep system and administrators in particular with the ability to identify and control the state of users logged into the planrep system. The user management is among the core part of the mainly redesigned planrep as it allows to identify and group users according to their allocations/specialization and responsibilities.</p>
<p align="justify">The user management has the following tables</p>

<ol>
	<li><a href="#accessrights" title="access_rights">access_rights</a></li>
	<li><a href="#roles" title="roles">roles</a></li>
	<li><a href="#users" title="users">users</a></li>
	<li><a href="#activations" title="activations">activations</a></li>
	<li><a href="#role_users" title="role_users">role_users</a></li>
	<li><a href="#user_profile_keys" title="user_profile_keys">user_profile_keys</a></li>
	<li><a href="#user_titles" title="user_titles">user_titles</a></li>
	<li><a href="#user_groups" title="user_groups">user_groups</a></li>
	<li><a href="#user_profiles" title="user_profiles">user_profiles</a></li>
	<li><a href="#throttle" title="throttle">throttle</a></li>
	<li><a href="#group_rights" title="group_rights">group_rights</a></li>
</ol>

## <a name="accessrights"></a><u>access_rights</u>
<p align="justify">This table store information about the available access rights that can be assigned to a user. Eg [view, read] certain reports, pages etc</p>

![access_rights](../img/usermanagement/access_rights.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#roles" title="Next Table">Next</a>

## <a name="roles"></a><u>roles</u>
<p align="justify">This table store information with the name of the defined roles within the system. Usually a role is accompanied with a set of access rights to different pages/menus within planrep</p>

![roles](../img/usermanagement/roles.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#users" title="Go Top">Next</a>

## <a name="users"></a><u>users</u>
<p align="justify">This table is the one which stores all the user accounts of PlanRep.</p>

![users](../img/usermanagement/users.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#activations" title="Go Top">Next</a>

## <a name="activations"></a><u>activations</u>
<p align="justify">This table store information that will be used to control user account statuses</p>

![activations](../img/usermanagement/activations.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#role_users" title="Go Top">Next</a>

## <a name="role_users"></a><u>role_users</u>
<p align="justify">This table store information for user and their respective roles.</p>

![role_users](../img/usermanagement/role_users.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#user_profile_keys" title="Go Top">Next</a>

## <a name="user_profile_keys"></a><u>user_profile_keys</u>
<p align="justify">This table store information for the user's profile informations keys. This will be helpful in auditing and transactions tracking</p>

![user_profile_keys](../img/usermanagement/user_profile_keys.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#user_titles" title="Go Top">Next</a>

## <a name="user_titles"></a><u>user_titles</u>
<p align="justify">This table store information about users title</p>

![user_titles](../img/usermanagement/user_titles.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#user_groups" title="Go Top">Next</a>

## <a name="user_groups"></a><u>user_groups</u>
<p align="justify">This table store information concerning the different groups of users that  will be privileged with certain roles, rights etc </p>

![user_groups](../img/usermanagement/user_groups.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#user_profiles" title="Go Top">Next</a>

## <a name="user_profiles"></a><u>user_profiles</u>
<p align="justify">This table store information about user profiles.</p>

![user_profiles](../img/usermanagement/user_profiles.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#throttle" title="Go Top">Next</a>

## <a name="throttle"></a><u>throttle</u>
<p align="justify">This table store information that are used to control the user management process, such as the number of logins etc </p>

![throttle](../img/usermanagement/throttle.png)

<a href="#usermanagement" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#group_rights" title="Go Top">Next</a>

## <a name="group_rights"></a><u>group_rights</u>
<p align="justify">This table store information about the group rights </p>

![group_rights](../img/usermanagement/group_rights.png)

<a href="#usermanagement" title="Go Top">Back Top</a>