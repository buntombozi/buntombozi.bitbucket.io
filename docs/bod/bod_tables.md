# <a name="bod-home" style="color: unset;">Burden of Disease Tables

This module has the following tables

<ol>
	<li><a href="#bodlist" title="bod_lists">bod_lists</a></li>
	<li><a href="#bodversion" title="bod_versions">bod_versions</a></li>
	<li><a href="#interventions" title="interventions">interventions</a></li>
	<li><a href="#bod-interventions" title="bod_interventions">bod_interventions</a></li>
	<li><a href="#intev_cat" title="intervention_gategories">intervention_categories</a></li>
	<li><a href="#fund_sources" title="fund_sources">fund_sources</a></li>
	<li><a href="#fs_categ" title="fund_source_categories">fund_source_categories</a></li>
	<li><a href="#fy" title="financial_years">financial_years</a></li>
</ol>

## <a name="bodlist"></a><u>bod_lists</u>
<p align="justify">This table deals with Burden of Disease. It keeps record for the list of burden of diseases used during the setup of  Burden of Disease Versions.</p>

![bod_lists](../img/bod/bod_lists.png)

<a href="#bod-home" title="Go Top">Back Top</a>

## <a name="bodversion"></a><u>bod_versions</u>
<p align="justify">This table deals with Burden of Disease. It keeps record for the list of burden of diseases Version that link with Burden of disease Intervention.</p>

![bod_versions](../img/bod/bod_versions.png)

<a href="#bod-home" title="Go Top">Back Top</a>

## <a name="interventions"></a><u>interventions</u>
<p align="justify">This  table store  the list of interventions  thats link with the priority areas and intervention category.</p>

![interventions](../img/bod/interventions.png)

<a href="#bod-home" title="Go Top">Back Top</a>

## <a name="bod-interventions"></a><u>bod_interventions</u>
<p align="justify">This  table deals with Burden of Disease. It keeps record for the list of Burden of Diseases  Interventions.</p>

![bod_interventions](../img/bod/bod_interventions.png)

<a href="#bod-home" title="Go Top">Back Top</a>

## <a name="intev_cat"></a><u>intervention_categories</u>
<p align="justify">This table store intervention categories details  that are  used during the setup of  the burden of diseases  intervention ,intervention categories link with  burden of disease version.</p>

![intervention_categories](../img/bod/intervention_categories.png)

<a href="#bod-home" title="Go Top">Back Top</a>

## <a name="fund_sources"></a><u>fund_sources</u>
<p align="justify">This table store information on fund sources .In burden of disease as sub module of setup is used during setting up intervention categories we must link them with fund sources so that to to understand  which funder funds the specific intervention category</p>

![fund_sources](../img/bod/fund_sources.png)

<a href="#bod-home" title="Go Top">Back Top</a>

## <a name="fs_categ"></a><u>fund_source_categories</u>
<p align="justify">Stores the categories of funds of a council. E.g. council own source, global funds, e.t.c</p>

![fund_source_categories](../img/bod/fund_source_categories.png)

<a href="#bod-home" title="Go Top">Back Top</a>

## <a name="fy"></a><u>financial_years</u>
<p align="justify">This is the table which stores the financial years of PlanRep. </p>

![financial_years](../img/bod/financial_years.png)

<a href="#bod-home" title="Go Top">Back Top</a>