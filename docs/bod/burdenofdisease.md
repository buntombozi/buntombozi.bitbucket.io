# Burden of Disease
<p align="justify">This module has been designed to measure the Burden of disease. It contains tables that store the list of Burden of disease,Burden of disease version and the Burden of diseases intervention.These data are entered on Setup module by a user from national level and the data are used to generate Burden of disease reports.</p>

## ER Diagram (Burden of Disease)
The ER diagram  shows how tables that forms and complete the BOD Module have been organised and how they relate, depend each other in accomplishing the tasks under the BOD Module. 

![BOD Module ER Diagram](../img/bod/BOD.png)
