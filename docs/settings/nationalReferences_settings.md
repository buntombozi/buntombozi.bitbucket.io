# <a  name="national_reference" style="color: unset;">National Reference
<p align="justify">The national references setting are used by the planning module ….., the national reference settings helps planners to set <em>targets, objectives, activities</em>  according to national guidelines that are produced in national reference </p>
<p align="justify"> The national references  are recorded in a table called <code><a href="#reference_docs" title="reference_docs">reference_docs</a></code></p>

## <a name="reference_docs"></a><u>reference_docs</u>
<p align="justify">This table store information about the references to whom a planner must reference during planning processes. It is used by the planning module. Eg of references that are being stored by <code>refence_docs</code> table are <em>Ruling Party manifesto, SDG, Development Plan </em>etc</p>

![reference_docs](../img/settings/national_reference/reference_docs.png)

<a href="#national_reference" title="Go Top">Back Top</a>