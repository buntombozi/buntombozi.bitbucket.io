# <a  name="comprehensive_plans_setups" style="color: unset;">Comprehensive Plans Setups
<p align="justify">The comprehensive plans settings are intended to set an environment during data entry and data visualization for the comprehensive plans</p>

<p align="justify"> Comprehensive Plans Setups has the following tables</p>
<ol>
	<li><a href="#cas_plan_table_select_options" title="cas_plan_table_select_options">cas_plan_table_select_options</a></li>
	<li><a href="#baseline_statistics" title="baseline_statistics">baseline_statistics</a></li>
</ol>

## <a name="cas_plan_table_select_options"></a><u>cas_plan_table_select_options</u>
<p align="justify">This table store the select options fro the comprehensive plans</p>

![cas_plan_table_select_options](../img/settings/planning_settings/cas_plan_table_select_options.png)

<a href="#comprehensive_plans_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#baseline_statistics" title="Next Table">Next</a>

## <a name="baseline_statistics"></a><u>baseline_statistics</u>
<p align="justify">This table store the baseline statics information that will be referred to.  Eg. <em>Population, Exchange rates, Projections etc</em></p>

![baseline_statistics](../img/settings/planning_settings/baseline_statistics.png)

<a href="#comprehensive_plans_setups" title="Go Top">Back Top</a>