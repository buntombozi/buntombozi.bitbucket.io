# <a  name="sector_problems_setups" style="color: unset;">Sector Problems Settings
<p align="justify">The sector problems setting are used by the planning module specify information about the specific sector problems/challenges. From this challenges the sector can plans on how to progress on addressing the challenges/problems. This enables the planners to concentrate on addressing a sector specific problems</p>
<p align="justify"> The sector problems  are recorded in a table called <code><strong><a href="#generic_sector_problems" title="generic_sector_problems">generic_sector_problems</a></strong></code>.</p>

## <a name="generic_sector_problems"></a><u>generic_sector_problems</u>
<p align="justify">This table store generic sector problems that have been identified by a specific planning institutions according to their situations. This my not be necessarily a nationwide problem for a sector but it can be very specific for a certain sector and a certain place </p>

![generic_sector_problems](../img/settings/planning_settings/generic_sector_problems.png)

<a href="#sector_problems_setups" title="Go Top">Back Top</a>