# <a  name="planning_settings" style="color: unset;">Planning Settings
<p align="justify">The planning settings are important order to allow planners and end users to execute their responsibilities. Under planning settings there are also sub-modules settings such as.</p>
<code>
<ul style="list-style-type: square; color: none">
	<li>Performance Indicators</li>
	<li>Priority areas</li>
	<li>Comprehensive plans</li>
	<li>Transport facilities</li>
</ul>
</code>

<p align="justify"> The planning settings has the following tables</p>
<ol>
	<li><a href="#activity_task_natures" title="activity_task_natures">activity_task_natures</a></li>
	<li><a href="#activity_statuses" title="activity_statuses">activity_statuses</a></li>
	<li><a href="#reference_document_types" title="reference_document_types">reference_document_types</a></li>
	<li><a href="#guidelines" title="guidelines">guidelines</a></li>
	<li><a href="#plan_chain_types" title="plan_chain_types">plan_chain_types</a></li>
	<li><a href="#plan_chains" title="plan_chains">plan_chains</a></li>
	<li><a href="#plan_chain_sectors" title="plan_chain_sectors">plan_chain_sectors</a></li>
	<li><a href="#activity_funders" title="activity_funders">activity_funders</a></li>
	<li><a href="#activity_indicator_data" title="activity_indicator_data">activity_indicator_data</a></li>
	<li><a href="#activity_indicator_guidelines" title="activity_indicator_guidelines">activity_indicator_guidelines</a></li>
	<li><a href="#activity_indicators" title="activity_indicators">activity_indicators</a></li>
	<li><a href="#activity_references" title="activity_references">activity_references</a></li>
	<li><a href="#long_term_target_references" title="long_term_target_references">long_term_target_references</a></li>
	<li><a href="#reference_types" title="reference_types">reference_types</a></li>
	<li><a href="#sector_fund_sources" title="sector_fund_sources">sector_fund_sources</a></li>
	<li><a href="#sector_problems" title="sector_problems">sector_problems</a></li>
	<li><a href="#sector_projects" title="sector_projects">sector_projects</a></li>
	<li><a href="#target_performance_indicators" title="target_performance_indicators">target_performance_indicators</a></li>
	<li><a href="#target_types" title="target_types">target_types</a></li>
	<li><a href="#planning_units" title="planning_units">planning_units</a></li>
</ol>

## <a name="activity_task_natures"></a><u>activity_task_natures</u>
<p align="justify">This table is intended to store information on nature of all activities that will be planned. All activities that are supposed to be carried by planners have been categorized and given a common nature. Eg <em>Training, Rehabilitation, Supervision etc</em> </p>

![activity_task_natures](../img/settings/planning_settings/activity_task_natures.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#activity_statuses" title="Next Table">Next</a>

## <a name="activity_statuses"></a><u>activity_statuses</u>
<p align="justify">This table store the statutes for the activity that has been registered and planned. The statutes are important especially for the execution/implementation module. Usually the activity statuses will be like <em><strong>Complete, Ongoing, Finished, Not Yet Started etc</strong></em></p>

![activity_statuses](../img/settings/planning_settings/activity_statuses.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#reference_document_types" title="Go Top">Next</a>

## <a name="reference_document_types"></a><u>reference_document_types</u>
<p align="justify">This table store information on the type of references documents that are being used by planners to refer to when administering their plans. Eg. reference document type  is  <code><strong>Strategic Plan, Party Manifesto, Assessment Guideline etc.</strong></code></p>

![reference_document_types](../img/settings/planning_settings/reference_document_types.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#activity_indicators" title="Go Top">Next</a>

## <a name="guidelines"></a><u>guidelines</u>
<p align="justify">This table store information about the guidelines that will be used during planning and other official operations within planrep. All activities/operations by users especially for the planning modules needs to be directed from a certain guideline.</p>

![guidelines](../img/settings/planning_settings/guidelines.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#plan_chain_types" title="Go Top">Next</a>

## <a name="plan_chain_types"></a><u>plan_chain_types</u>
<p align="justify">This table is intended to store information about the types  of chains and flows the planning module depends. The flow of plans follows the type from <em>Objectives, Targets, Service Outputs etc.</em></p>

![plan_chain_types](../img/settings/planning_settings/plan_chain_types.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#plan_chains" title="Go Top">Next</a>

## <a name="plan_chains"></a><u>plan_chains</u>
<p align="justify">This table is intended to store all plans chains. The plans will be planned on derivate sequence based on the plan chains type</p>

![plan_chains](../img/settings/planning_settings/plan_chains.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#plan_chain_sectors" title="Go Top">Next</a>

## <a name="plan_chain_sectors"></a><u>plan_chain_sectors</u>
<p align="justify">This table maps informations from <code><strong>plan_chains</strong></code> and <code><strong>sectors</strong></code>  tables and filter the plan chains in grouped and mapped formats in sector-wise basis. This helps in leveraging the operations on a more sector wise analysis. The advantage of this is that a user belonging to a certain sector won't be required to see plans and other inputs from other sectors.</p>

![plan_chain_sectors](../img/settings/planning_settings/plan_chain_sectors.png)

<a href="#planning_settings" title="Go Top">Back Top</a>&nbsp; &nbsp; |&nbsp; &nbsp; <a href="#activity_funders" title="Go Top">Next</a>

## <a name="activity_funders"></a><u>activity_funders</u>
<p align="justify">This table store information about the funders that are specifically dedicated to fund a certain activity. Used by the planning module.</p>

![activity_funders](../img/settings/planning_settings/activity_funders.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#activity_indicator_data" title="Next Table">Next</a>

## <a name="activity_indicator_data"></a><u>activity_indicator_data</u>
<p align="justify">This table store information about types of facilities. The facility type informations helps in identification (according to guidelines and policy) the roles the facilities will play in planning, budgeting, scrutinisation etc.. Eg of facility types are Hospital, Schools</p>

![activity_indicator_data](../img/settings/planning_settings/activity_indicator_data.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#users" title="Go Top">Next</a>

## <a name="activity_indicator_guidelines"></a><u>activity_indicator_guidelines</u>
<p align="justify">This table store guidelines and policies about activity indicators. The information on this table are mapped from <code>guidelines</code> and <code>activity_indicators</code> tables. All activities indicators must have guideline on how to calculate/measure it.</p>

![activity_indicator_guidelines](../img/settings/planning_settings/activity_indicator_guidelines.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#activity_indicators" title="Go Top">Next</a>

## <a name="activity_indicators"></a><u>activity_indicators</u>
<p align="justify">This table store information about the activity indicators. Activities are supposed to have indicators to measure their accomplishment (implementation)</p>

![activity_indicators](../img/settings/planning_settings/activity_indicators.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#activity_references" title="Go Top">Next</a>

## <a name="activity_references"></a><u>activity_references</u>
<p align="justify">This table store informations about the activity references. It holds mapped information from the <code>activities</code> and <code>reference_docs</code> tables</p>

![activity_references](../img/settings/planning_settings/activity_references.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#long_term_target_references" title="Go Top">Next</a>

## <a name="long_term_target_references"></a><u>long_term_target_references</u>
<p align="justify">This table store information for references where the long term targets might be derived. This table maps and ties informations from the <code>long_term_targets</code> and <code>reference_docs</code> tables. </p>

![long_term_target_references](../img/settings/planning_settings/long_term_target_references.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#reference_types" title="Go Top">Next</a>

## <a name="reference_types"></a><u>reference_types</u>
<p align="justify">This table store information about the type of references that can be used in planrep by planners and other users in the planning circle. Eg of references type documents are <em>Ruling Party's Manifesto, Sustainable Development Goals(SDG) </em>etc</p>

![reference_types](../img/settings/planning_settings/reference_types.png)

<a href="#planning_settings" title="Go Top">Back Top</a>

## <a name="sector_fund_sources"></a><u>sector_fund_sources</u>
<p align="justify">This table store information about the fund sources that are dedicated to a given sector specific. This table stores mapped informations from <code>fund_sources</code> and <code>sectors</code> tables</p>

![sector_fund_sources](../img/settings/planning_settings/sector_fund_sources.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#sector_problems" title="Next Table">Next</a>

## <a name="sector_problems"></a><u>sector_problems</u>
<p align="justify">This table store information about the specific sector problems/challenges. From this challenges the sector can plans on how to progress on addressing the challenges/problems. This enables the planners to concentrate on addressing a sector specific problems</p>

![sector_problems](../img/settings/planning_settings/sector_problems.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#users" title="Go Top">Next</a>

## <a name="sector_projects"></a><u>sector_projects</u>
<p align="justify">This table store information about sector specific projects Eg. Health, Agriculture. A user assigned to a certain sector will see first the projects that are only specific for the sectors. This table store mapped informations from the <code>sectors</code> and <code>projects</code> table.</p>

![sector_projects](../img/settings/planning_settings/sector_projects.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#target_performance_indicators" title="Go Top">Next</a>

## <a name="target_performance_indicators"></a><u>target_performance_indicators</u>
<p align="justify">This table store mapped informations from the <code>long_term_targets</code>  and <code>performance_indicators</code> tables.</p>

![target_performance_indicators](../img/settings/planning_settings/target_performance_indicators.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#target_types" title="Go Top">Next</a>

## <a name="target_types"></a><u>target_types</u>
<p align="justify">This table store information type of targets.</p>

![target_types](../img/settings/planning_settings/target_types.png)

<a href="#planning_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#planning_units" title="Go Top">Next</a>

## <a name="planning_units"></a><u>planning_units</u>
<p align="justify">This table store information of planning units</p>

![planning_units](../img/settings/planning_settings/planning_units.png)

<a href="#planning_settings" title="Go Top">Back Top</a>