# <a name="facility_settings" style="color: unset;"> Facility Setting
<p align="justify">The facility settings are the prior setting for the facility planning, CCHP, assessment etc. The settings are important since they will enable the facilities to administer their plans according to guidelines and policies. The facilities planning are among of the new features that has been introduced to the newly redesigned planrep.</p>

## <code>Facility Settings Schema</code>

![facility_settings](../img/general_schema/facility_settings.png)

<p align="justify">The facility settings has the following tables</p>

<ol>
	<li><a href="#lga_levels" title="lga_levels">lga_levels</a></li>
	<li><a href="#facility_types" title="facility_types">facility_types</a></li>
	<li><a href="#facility_ownerships" title="facility_ownerships">facility_ownerships</a></li>
	<li><a href="#facility_physical_states" title="facility_physical_states">facility_physical_states</a></li>
	<li><a href="#facility_star_ratings" title="facility_star_ratings">facility_star_ratings</a></li>
	<li><a href="#facility_custom_details" title="facility_custom_details">facility_custom_details</a></li>
	<li><a href="#facility_custom_details_mappings" title="facility_custom_details_mappings">facility_custom_details_mappings</a></li>
</ol>

## <a name="lga_levels"></a><u>lga_levels</u>
<p align="justify">This table store information on levels relationships (supervision roles) in order to allow facilities planning and other processes taking place in planrep system. Eg. <em>Council HQ → High Level | Dispensaries → Lower Level.</em> </p>

![lga_levels](../img/settings/facility/lga_levels.png)

<a href="#facility_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#facility_types" title="Next Table">Next</a>

## <a name="facility_types"></a><u>facility_types</u>
<p align="justify">This table store information about types of facilities. The facility type informations helps in identification (according to guidelines and policy) the roles the facilities will play in planning, budgeting, scrutinisation etc.. Eg of facility types are Hospital, Schools</p>

![facility_types](../img/settings/facility/facility_types.png)

<a href="#facility_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#users" title="Go Top">Next</a>

## <a name="facility_ownerships"></a><u>facility_ownerships</u>
<p align="justify">This table stores types of facility ownerships say <strong>Public or Private;</strong> which is used when creating facilities in Setup Module.</p>

![](../img/settings/facility/facility_ownerships.png)

<a href="#facility_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#facility_physical_states" title="Go Top">Next</a>

## <a name="facility_physical_states"></a><u>facility_physical_states</u>
<p align="justify">This table stores categories of facility physical states say <strong>Good, Minor Rehabilitation needed, Major Rehabilitation needed etc;</strong> which is used when creating facilities in Setup Module.</p>

![facility_physical_states](../img/settings/facility/facility_physical_states.png)

<a href="#facility_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#facility_star_ratings" title="Go Top">Next</a>

## <a name="facility_star_ratings"></a><u>facility_star_ratings</u>
<p align="justify">This table stores facility star ratings say like <strong>0 - Star, 1- Star, 2 - Stars etc;</strong> which is used when creating facilities especially Health  sector facilities  in Setup Module.</p>

![facility_star_ratings](../img/settings/facility/facility_star_ratings.png)

<a href="#facility_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#facility_custom_details" title="Go Top">Next</a>

## <a name="facility_custom_details"></a><u>facility_custom_details</u>
<p align="justify">This table stores extra facility details say which are used when creating facilities in Setup Module. The custom details provides and describes the capacity of the facility on service delivery</p>

![facility_custom_details](../img/settings/facility/facility_custom_details.png)

<a href="#facility_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#facility_custom_details_mappings" title="Go Top">Next</a>

## <a name="facility_custom_details_mappings"></a><u>facility_custom_details_mappings</u>
<p align="justify">This table stores mappend information from the <em><strong>facility_types, facility_custom_details, facility_ownerships</strong></em>which are used when creating facilities in Setup Module. It combines and maps informations from the three tables and put them into a combined meaningfully complete information</p>

![facility_custom_details_mappings](../img/settings/facility/facility_custom_details_mappings.png)

<a href="#facility_settings" title="Go Top">Back Top</a>