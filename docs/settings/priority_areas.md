# <a  name="priority_areas_settings" style="color: unset;">Priority Areas
<p align="justify">The priority areas guides planners during planning process on what should be the priority areas. After the priority areas has been identified then the planners will continue to set the interventions that will be applied to address the identified priority areas</p>
<p align="justify">The schema diagram below show how the <code>priority areas</code> and <code>interventions</code> are administered and managed in the planrep system</p>

## <code>Priority Areas Schema Diagram</code>
![priority_areas_schema](../img/settings/planning_settings/priority_areas_schema.png)

<p align="justify"> Priority areas has the following tables</p>
<ol>
	<li><a href="#link_levels" title="link_levels">link_levels</a></li>
	<li><a href="#priority_areas" title="priority_areas">priority_areas</a></li>
	<li><a href="#intervention_categories" title="intervention_categories">intervention_categories</a></li>
	<li><a href="#interventions" title="interventions">interventions</a></li>
</ol>

## <a name="link_levels"></a><u>link_levels</u>
<p align="justify">This table indicates the link the priority areas are referenced to. Eg <em>Target, Activity .. etc</em></p>

![link_levels](../img/settings/planning_settings/link_levels.png)

<a href="#priority_areas_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#priority_areas" title="Next Table">Next</a>

## <a name="priority_areas"></a><u>priority_areas</u>
<p align="justify">This table store informations about priority areas for planners to look for when doing planning.</p>

![priority_areas](../img/settings/planning_settings/priority_areas.png)

<a href="#priority_areas_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#intervention_categories" title="Next Table">Next</a>

## <a name="intervention_categories"></a><u>intervention_categories</u>
<p align="justify">This table store intervention categories details  that are  used during the setup of  the burden of diseases  intervention ,intervention categories link with  burden of disease version. <br /> <strong>NB:</strong><em>This table is used by the Durden of Disease Schema</em> </p>

![intervention_categories](../img/settings/planning_settings/intervention_categories.png)

<a href="#priority_areas_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#interventions" title="Go Top">Next</a>

## <a name="interventions"></a><u>interventions</u>
<p align="justify">This  table store  the list of interventions  thats link with the priority areas and intervention category.</strong></code></p>

![interventions](../img/settings/planning_settings/interventions.png)

<a href="#priority_areas_settings" title="Go Top">Back Top</a>