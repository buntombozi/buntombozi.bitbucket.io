# <a  name="generic_activities_setups" style="color: unset;">Generic Activities
<p align="justify">The generic activities setting are used by the planning module. The generic activities settings helps the local planners to set activities according to targets based on their localities. </p>
<p align="justify"> The generic activities  are recorded in a table called  <code><strong><a href="#generic_activities" title="generic_activities">generic_activities</a></strong></code>.</p>

## <a name="generic_activities"></a><u>generic_activities</u>
<p align="justify">This table store generic activities. The generic activities may be specific for a certain planning institution/LGA. This table also maps/(chronological chain) informations from other tables such as <code>generic_targets, interventions, budget_classes, generic_sector_problems, performance_indicators, national_targets</code>. This mapping enable the planner to chains the generic activities accordingly.</code></p>

![generic_activities](../img/settings/planning_settings/generic_activities.png)

<a href="#generic_activities_setups" title="Go Top">Back Top</a>