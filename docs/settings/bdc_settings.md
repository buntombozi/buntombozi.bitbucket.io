# <a  name="bdc_setups" style="color: unset;">Budget Distribution Settings
<p align="justify">The budget distribution settings are sub-modules under the budget settings. The settings deal with budget distributions that will be applied to planned activities etc </p>

## Budget Distribution Schema
<p align="justify">This is the schema for the budget distibution settings</p>
![bdc_schema](../img/settings/budget_settings/bdc_schema.png)

<p align="justify">The budget distribution settings has the following tables</p>

<ol>
	<li><a href="#bdc_expenditure_centre_groups" title="bdc_expenditure_centre_groups">bdc_expenditure_centre_groups</a></li>
	<li><a href="#bdc_expenditure_centres" title="bdc_expenditure_centres">bdc_expenditure_centres</a></li>
	<li><a href="#bdc_sector_expenditure_sub_centres" title="bdc_sector_expenditure_sub_centres">bdc_sector_expenditure_sub_centres</a></li>
	<li><a href="#bdc_main_groups" title="bdc_main_groups">bdc_main_groups</a></li>
	<li><a href="#bdc_main_group_fund_sources" title="bdc_main_group_fund_sources">bdc_main_group_fund_sources</a></li>
	<li><a href="#bdc_groups" title="bdc_groups">bdc_groups</a></li>
	<li><a href="#bdc_sector_expenditure_sub_center_groups" title="bdc_sector_expenditure_sub_center_groups">bdc_sector_expenditure_sub_center_groups</a></li>
	<li><a href="#bdc_expenditure_centre_links" title="bdc_expenditure_centre_links">bdc_expenditure_centre_links</a></li>
	<li><a href="#link_specs" title="link_specs">link_specs</a></li>
	<li><a href="#bdc_sector_expenditure_sub_centre_gfs_codes" title="bdc_sector_expenditure_sub_centre_gfs_codes">bdc_sector_expenditure_sub_centre_gfs_codes</a></li>
</ol>

## <a name="bdc_expenditure_centre_groups"></a><u>bdc_expenditure_centre_groups</u>
<p align="justify">This table store information of the budget distribution centre groups.</p>

![bdc_expenditure_centre_groups](../img/settings/budget_settings/bdc_expenditure_centre_groups.png)

<a href="#bdc_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#bdc_expenditure_centres" title="Next Table">Next</a>

## <a name="bdc_expenditure_centres"></a><u>bdc_expenditure_centres</u>
<p align="justify">This table store information for the expenditure centres.</p>

![bdc_expenditure_centres](../img/settings/budget_settings/bdc_expenditure_centres.png)

<a href="#bdc_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#bdc_sector_expenditure_sub_centres" title="Go Top">Next</a>

## <a name="bdc_sector_expenditure_sub_centres"></a><u>bdc_sector_expenditure_sub_centres</u>
<p align="justify">This table store information sector expenditure sub centres..</p>

![bdc_sector_expenditure_sub_centres](../img/settings/budget_settings/bdc_sector_expenditure_sub_centres.png)

<a href="#bdc_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#bdc_main_groups" title="Go Top">Next</a>

## <a name="bdc_main_groups"></a><u>bdc_main_groups</u>
<p align="justify">This table store informations about the budget distribution condition main groups</p>

![bdc_main_groups](../img/settings/budget_settings/bdc_main_groups.png)

<a href="#bdc_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#bdc_main_group_fund_sources" title="Go Top">Next</a>

## <a name="bdc_main_group_fund_sources"></a><u>bdc_main_group_fund_sources</u>
<p align="justify">This table store information about the fund sources relating to the budget distribution conditions main groups.</p>

![bdc_main_group_fund_sources](../img/settings/budget_settings/bdc_main_group_fund_sources.png)

<a href="#bdc_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#bdc_groups" title="Go Top">Next</a>

## <a name="bdc_groups"></a><u>bdc_groups</u>
<p align="justify">This table store information  the groups for the budget distribution condition</p>

![bdc_groups](../img/settings/budget_settings/bdc_groups.png)

<a href="#bdc_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#bdc_sector_expenditure_sub_center_groups" title="Go Top">Next</a>

## <a name="bdc_sector_expenditure_sub_center_groups"></a><u>bdc_sector_expenditure_sub_center_groups</u>
<p align="justify">This table store the mapping information from  the <code>bdc_groups</code>  and <code>bdc_sector_expenditure_sub_centre</code> tables</p>

![bdc_sector_expenditure_sub_center_groups](../img/settings/budget_settings/bdc_sector_expenditure_sub_center_groups.png)

<a href="#bdc_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#bdc_expenditure_centre_links" title="Go Top">Next</a>

## <a name="bdc_expenditure_centre_links"></a><u>bdc_expenditure_centre_links</u>
<p align="justify">This table store mapped information for <code>gfs_codes</code> and <code>expenditure_centres</code> tables</p>

![bdc_expenditure_centre_links](../img/settings/budget_settings/bdc_expenditure_centre_links.png)

<a href="#bdc_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#bdc_sector_expenditure_sub_centre_gfs_codes" title="Go Top">Next</a>

## <a name="bdc_sector_expenditure_sub_centre_gfs_codes"></a><u>bdc_sector_expenditure_sub_centre_gfs_codes</u>
<p align="justify">This table store mapped information from <code>bdc_sector_expenditure_sub_centres</code> and <code>gfs_codes tables</code>.</p>

![bdc_sector_expenditure_sub_centre_gfs_codes](../img/settings/budget_settings/bdc_sector_expenditure_sub_centre_gfs_codes.png)

<a href="#bdc_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#link_specs" title="Go Top">Next</a>


## <a name="link_specs"></a><u>link_specs</u>
<p align="justify">This table store informations about the template that is being used by different budget classes.</p>

![link_specs](../img/settings/budget_settings/link_specs.png)

<a href="#bdc_setups" title="Go Top">Back Top</a>