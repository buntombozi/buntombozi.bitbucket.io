# <a  name="gfs_codes" style="color: unset;">GFS Code Management Settings
<p align="justify">The GFS Code Setting Management settings allows to set parameters that allows analysis by type of expenditure or revenue item during planning,  budgeting and later used in scrutinization and physical implementation.</p>

## <code>GFS CODES MANAGEMENT SCHEMA</code>

![gfs_codes](../img/general_schema/gfs_codes.png)

<p align="justify">The following are tables that has been used for gfs code management settings schema</p>
<ol>
	<li><a href="#account_types" title="account_types">account_types</a></li>
	<li><a href="#gfs_code_categories" title="gfs_code_categories">gfs_code_categories</a></li>
	<li><a href="#gfs_code_sub_categories" title="gfs_code_sub_categories">gfs_code_sub_categories</a></li>
	<li><a href="#gfs_codes" title="gfs_codes">gfs_codes</a></li>
	<li><a href="#procurement_types" title="procurement_types">procurement_types</a></li>
	<li><a href="#units" title="units">units</a></li>
	<li><a href="#ceiling_gfs_codes" title="ceiling_gfs_codes">ceiling_gfs_codes</a></li>
	<li><a href="#gfs_code_sections" title="gfs_code_sections">gfs_code_sections</a></li>
	<li><a href="#admin_hierarchy_gfs_codes" title="admin_hierarchy_gfs_codes">admin_hierarchy_gfs_codes</a></li>
</ol>

## <a name="account_types"></a><u>account_types</u>
<p align="justify">This table stores GFS Code account types (Revenue, Assets, Expenses etc)</p>

![account_types](../img/settings/gfs_codes/account_types.png)

<a href="#gfs_codes" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#gfs_code_categories" title="Next Table">Next</a>

## <a name="gfs_code_categories"></a><u>gfs_code_categories</u>
<p align="justify">This table stores GFS Code Categories which are used to categorize GFS Codes</p>

![gfs_code_categories](../img/settings/gfs_codes/gfs_code_categories.png)

<a href="#gfs_codes" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#gfs_code_sub_categories" title="Go Top">Next</a>

## <a name="gfs_code_sub_categories"></a><u>gfs_code_sub_categories</u>
<p align="justify">This table stores GFS Code Categories which are used to categorize GFS Codes.</p>

![gfs_code_sub_categories](../img/settings/gfs_codes/gfs_code_sub_categories.png)

<a href="#gfs_codes" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#gfs_codes" title="Go Top">Next</a>

## <a name="gfs_codes"></a><u>gfs_codes</u>
<p align="justify">This table stores GFS Codes. GFS Codes are used in planning &amp; budgeting. </p>

![gfs_codes](../img/settings/gfs_codes/gfs_codes.png)

<a href="#gfs_codes" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#procurement_types" title="Go Top">Next</a>

## <a name="procurement_types"></a><u>procurement_types</u>
<p align="justify">Procurement types table stores categories of purchase procurement types for budget input items which needs procurement later in the financial year</p>

![procurement_types](../img/settings/gfs_codes/procurement_types.png)

<a href="#gfs_codes" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#units" title="Go Top">Next</a>

## <a name="units"></a><u>units</u>
<p align="justify">Units table stores units of measure like Kilogram, Allowance, Lump Sum, Quarterly etc. Units of measure are used in planning &amp; budgeting for quantifying and prices of inputs.</p>

![units](../img/settings/gfs_codes/units.png)

<a href="#gfs_codes" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#ceiling_gfs_codes" title="Go Top">Next</a>

## <a name="ceiling_gfs_codes"></a><u>ceiling_gfs_codes</u>
<p align="justify">This table store ceiling informations for a gfs codes. It is used in budgeting module</p>

![ceiling_gfs_codes](../img/settings/gfs_codes/ceiling_gfs_codes.png)

<a href="#gfs_codes" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#gfs_code_sections" title="Go Top">Next</a>

## <a name="gfs_code_sections"></a><u>gfs_code_sections</u>
<p align="justify">This table store referenced information from the gfs_codes and sections table in order to map the gfs codes that are dedicated to a certain section/planning units such as <em><strong>Health - Curative, Education - Primary Education etc</strong></em></p>

![gfs_code_sections](../img/settings/gfs_codes/gfs_code_sections.png)

<a href="#gfs_codes" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#admin_hierarchy_gfs_codes" title="Go Top">Next</a>

## <a name="admin_hierarchy_gfs_codes"></a><u>admin_hierarchy_gfs_codes</u>
<p align="justify">This table maps informations between the <em><strong>admin_hierarchies</strong></em> and <em><strong>gfs_codes</strong></em> tables. The mapping  helps in identifying the gfs_codes in admin hierarchy perspectives and reduces the loading of unrelated gfs codes for different hierarchies.</p>

![admin_hierarchy_gfs_codes](../img/settings/gfs_codes/admin_hierarchy_gfs_codes.png)

<a href="#gfs_codes" title="Go Top">Back Top</a>