# <a  name="budget_settings_setups" style="color: unset;">Budget/Costing Settings
<p align="justify">The budget settings are prior settings needed by the budgeting module. The budget settings has sub-modules settings such as <code><strong>Budget Distribution , Personal emoluments(PE)</strong></code> and <code><strong>Assessor Assignments settings</strong></code>. </p>
<p align="justify">The budget settings has the following tables</p>

<ol>
	<li><a href="#ceiling_chains" title="ceiling_chains">ceiling_chains</a></li>
	<li><a href="#admin_hierarchy_lev_sec_mappings" title="admin_hierarchy_lev_sec_mappings">admin_hierarchy_lev_sec_mappings</a></li>
	<li><a href="#ceilings" title="ceilings">ceilings</a></li>
	<li><a href="#cash_accounts" title="cash_accounts">cash_accounts</a></li>
	<li><a href="#ceiling_sectors" title="ceiling_sectors">ceiling_sectors</a></li>
	<li><a href="#cash_account_balances" title="cash_account_balances">cash_account_balances</a></li>
	<li><a href="#fund_source_budget_classes" title="fund_source_budget_classes">fund_source_budget_classes</a></li>
	<li><a href="#funders" title="funders">funders</a></li>
	<li><a href="#budget_class_templates" title="budget_class_templates">budget_class_templates</a></li>
</ol>

## <a name="ceiling_chains"></a><u>ceiling_chains</u>
<p align="justify">This table store information about the ceiling chains flow of the ceiling distribution among the planning units within  an institutions or between administrative hierarchies. </p>

![ceiling_chains](../img/settings/budget_settings/ceiling_chains.png)

<a href="#budget_settings_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#admin_hierarchy_lev_sec_mappings" title="Next Table">Next</a>

## <a name="admin_hierarchy_lev_sec_mappings"></a><u>admin_hierarchy_lev_sec_mappings</u>
<p align="justify">This table stores mapping informations that enables to determine whether a planning unit/administrative hierarchy can perform certain tasks (Data entry or view only rights) in a budgeting module.</p>

![admin_hierarchy_lev_sec_mappings](../img/settings/budget_settings/admin_hierarchy_lev_sec_mappings.png)

<a href="#budget_settings_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#ceilings" title="Go Top">Next</a>

## <a name="ceilings"></a><u>ceilings</u>
<p align="justify">This table store informations about ceilings amounts. It is used mainly by the budgeting/costing module.</p>

![ceilings](../img/settings/budget_settings/ceilings.png)

<a href="#budget_settings_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#cash_accounts" title="Go Top">Next</a>

## <a name="cash_accounts"></a><u>cash_accounts</u>
<p align="justify">This table store cash account informations that can be used by the planning units</p>

![cash_accounts](../img/settings/budget_settings/cash_accounts.png)

<a href="#budget_settings_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#ceiling_sectors" title="Go Top">Next</a>

## <a name="ceiling_sectors"></a><u>ceiling_sectors</u>
<p align="justify">This table store mapped informations from <code>sectors</code> and <code>ceilings</code> tables. This information is usefully as it provides proper management of resources by describing those sectors that have ceilings and those that does not administer ceilings </p>

![ceiling_sectors](../img/settings/budget_settings/ceiling_sectors.png)

<a href="#budget_settings_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#cash_account_balances" title="Go Top">Next</a>

## <a name="cash_account_balances"></a><u>cash_account_balances</u>
<p align="justify">This table store information about the balances on the administrative units cash accounts that are used by the budgeting module. The table maps information from several tables such as <code>cash_accounts, admin_hierarchies</code> and <code>periods</code>.</p>

![cash_accounts_balances](../img/settings/budget_settings/cash_accounts_balances.png)

<a href="#budget_settings_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#fund_source_budget_classes" title="Go Top">Next</a>

## <a name="fund_source_budget_classes"></a><u>fund_source_budget_classes</u>
<p align="justify">This table store informations that determine the fund sources and their respective certain budget classes eg <em>Development - Recurrent, Revenue - Own source</em>. It maps informations from the <code>fund_sources</code>  and  <code>budget_classes</code> tables. </p>

![fund_source_budget_classes](../img/settings/budget_settings/fund_source_budget_classes.png)

<a href="#budget_settings_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#funders" title="Go Top">Next</a>

## <a name="funders"></a><u>funders</u>
<p align="justify">This table store funders informations</p>

![funders](../img/settings/budget_settings/funders.png)

<a href="#budget_settings_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#budget_class_templates" title="Go Top">Next</a>

## <a name="budget_class_templates"></a><u>budget_class_templates</u>
<p align="justify">This table store informations about the template that is being used by different budget classes.</p>

![budget_class_templates](../img/settings/budget_settings/budget_class_templates.png)

<a href="#budget_settings_setups" title="Go Top">Back Top</a>