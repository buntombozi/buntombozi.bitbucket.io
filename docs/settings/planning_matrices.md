# <a  name="planning_matrices_setups" style="color: unset;">Planning Matrices
<p align="justify">The planning matrices setting are used by the planning module. ….., the planning matrices setting ..….  </p>
<p align="justify"> The planning matrices  are recorded in a table called <code><strong><a href="#planning_matrices" title="planning_matrices">planning_matrices</a></strong></code>.</p>

## <a name="planning_matrices"></a><u>planning_matrices</u>
<p align="justify">This table store informations about the planning matrices. The planning matrices </p>

![planning_matrices](../img/settings/planning_settings/planning_matrices.png)

<a href="#planning_matrices_setups" title="Go Top">Back Top</a>