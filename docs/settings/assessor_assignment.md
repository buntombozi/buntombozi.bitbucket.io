# <a  name="assessor" style="color: unset;">Assessor Assignment
<p align="justify">The assessor assignment setting are used by scrutinization modules, assessment modules etc.</p>
<p align="justify"> The assessor assignment  are recorded in a table called <code><a href="#assessor_assignments" title="assessor_assignments">assessor_assignments</a></code></p>

## <a name="assessor_assignments"></a><u>assessor_assignments</u>
<p align="justify">This table stores planning assessment team for the regions and councils.</p>

![assessor_assignments](../img/settings/budget_settings/assessor_assignments.png)

<a href="#assessor" title="Go Top">Back Top</a>