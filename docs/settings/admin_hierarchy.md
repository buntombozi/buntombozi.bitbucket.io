# <a  name="administrative_home" style="color: unset;">Administrative Settings
<p align="justify">The administrative settings serves as a place to enter information such as administrative hierarchy <em><strong>(National, Regions, Districts, Wards, Village/Street, Hamlets etc)</strong></em>. Also under the administrative settings, the order of which the <em>decision</em> are made and flows within and organisation structure has been implemented. Eg the order of decision making in an LGAs follows the trend such as </p>

## <code>Planning Decision Making Order Trend</code>
<pre>--> <mark><strong>Council (Full)</strong> </mark>
[CMT consolidate plans --> Send to Full council for approval and submit to region] 
	--> <mark style="background-color: lightblue"><strong>Sectors</strong> </mark>
	[Sectoral Plans Scrutinisation Committees, Submit plans to council] 
		--> <mark style="background-color: pink"><strong>Department|Units</strong></mark> 
		[Ceiling Distribution, Consolidation of Plans and Budgets from Cost Centres, Submit to Sectors/Council] 
			--> <mark style="background-color: lightyellow"><strong>Cost Centres</strong> </mark>
			[Plans, Budgets/Costing and Execution]
				--> <mark style="background-color: lightgreen"><strong>Service Delivery Points</strong> </mark>
				[Plans and Submit the Plans to Cost Centres]</code></pre>

![organisation_chart](../img/general_schema/organisation_structure.png)

<p align="justify">The redesigned planrep has put much emphasis to ensure that the administrative settings will be flexible and scalable to align with policies and guidelines changes. For the redesigned planrep, most of the operations and functions such as <em>ceiling distribution, costing, scrutinization, assessment, execution, reporting etc </em> follows the administrative hierarchies. Hence proper parameters must be enforced when dealing with administrative settings for smooth operations of other planrep modules.</p>

## <code>Administrative Hierarchy Schema</code>
![administrative_hierarchies](../img/general_schema/administrative_hierarchies.png)

## <code>The Administrative Settings Tables</code>
The administrative settings employs the following tables
<ol>
	<li><a href="#adminhierarchylevels" title="admin_hierarchy_levels">admin_hierarchy_levels</a></li>
	<li><a href="#sectors" title="admin_hierarchies">admin_hierarchies</a></li>
	<li><a href="#decision_levels" title="decision_levels">decision_levels</a></li>
	<li><a href="#sectors" title="sectors">sectors</a></li>
	<li><a href="#section_levels" title="section_levels">section_levels</a></li>
	<li><a href="#sections" title="sections">sections</a></li>
	<li><a href="#calendar_events" title="calendar_events">calendar_events</a></li>
	<li><a href="#calendars" title="calendars">calendars</a></li>
	<li><a href="#calendar_event_reminder_recipients" title="calendar_event_reminder_recipients">calendar_event_reminder_recipients</a></li>
	<li><a href="#configuration_settings" title="configuration_settings">configuration_settings</a></li>
	<li><a href="#admin_hierarchy_parents" title="admin_hierarchy_parents">admin_hierarchy_parents</a></li>
</ol> 

## <a name="adminhierarchylevels"></a><u>admin_hierarchy_levels</u>
<p align="justify">This table stores all the administrative levels used in PlanRep. E.g Ministry, Region, Ward, Village e.t.c.</p>

![admin_hierarchies](../img/settings/administrative/admin_hierarchies.png)

<a href="#administrative_home" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#admin_hierarchies" title="Go Top">Next</a>

## <a name="admin_hierarchies"></a><u>admin_hierarchies</u>
<p align="justify">This table stores the administrative areas used in PlanRep e.g. Dar es salaam, Dodoma, Dodoma Municipal Council, e.t.c</p>

![](../img/settings/administrative/admin_hierarchies.png)

<a href="#administrative_home" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#decision_levels" title="Go Top">Next</a>

## <a name="decision_levels"></a><u>decision_levels</u>
<p align="justify">This table stores all the decision levels used in PlanRep in the planning and budgeting process. This table is also used during the process of budget scrutinization to see the decision trending and protocols from one level to another in pyramidical way.</p>

![decision_levels](../img/settings/administrative/decision_levels.png)

<a href="#administrative_home" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#sectors" title="Go Top">Next</a>

## <a name="sectors"></a><u>sectors</u>
<p align="justify">This table stores the sectors used in PlanRep and for planning and budgeting  purposes</p>

![sectors](../img/settings/administrative/sectors.png)

<a href="#administrative_home" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#section_levels" title="Go Top">Next</a>

## <a name="section_levels"></a><u>section_levels</u>
<p align="justify">This table is used to create a hierarchical definition of the sections/planning units that are important for all the planrep key and core functionalities modules such as planning, assessment, scrutinisation etc.</p>

![section_levels](../img/settings/administrative/section_levels.png)

<a href="#administrative_home" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#sections" title="Go Top">Next</a>

## <a name="sections"></a><u>sections</u>
<p align="justify">This table stores all the units/departments that are used in the planning, budgeting, scrutinization, assessment processes in PlanRep. It is a pyramidic hierarchy  Eg of the sections are <em><strong>Council >> Sectors >> Department/Unit >> Cost Centres >> Facilities</strong></em></p>

![sections](../img/settings/administrative/sections.png)

<a href="#administrative_home" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#calendar_events" title="Go Top">Next</a>

## <a name="calendar_events"></a><u>calendar_events</u>
<p align="justify">This table store information regarding the dates and periods that are intended to be crucial on the planrep operations. Eg Closing and Opening of Planning Circle, Budgeting, Scrutinization, Budget Submission etc</p>

![calendar_events](../img/settings/administrative/calendar_events.png)

<a href="#administrative_home" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#calendars" title="Go Top">Next</a>

## <a name="calendars"></a><u>calendars</u>
<p align="justify">This table store information for collective actions  on an a given event that will be used in planrep.  Eg. calendar for CCHP Assessment</p>

![calendars](../img/settings/administrative/calendars.png)

<a href="#administrative_home" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#calendar_event_reminder_recipients" title="Go Top">Next</a>

## <a name="calendar_event_reminder_recipients"></a><u>calendar_event_reminder_recipients</u>
<p align="justify">This table store reminders  informations that will be used for monitoring and administering of calendar events such as when the planning circle will start and end. The reminders will be sent to responsible person via mobile or emails. </p>

![calendar_event_reminder_recipients](../img/settings/administrative/calendar_event_reminder_recipients.png)

<a href="#administrative_home" title="Go Top">Back Top</a>