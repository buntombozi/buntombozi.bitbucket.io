# <a  name="project_data_forms_setups" style="color: unset;">Project Data Forms
<p align="justify">The project data forms are settings needed by planning module to set pre-condition on dealing with projects. The project data forms are used by the modules such as <code>planning, budgeting, assessment, scrutinization</code> etc.</p>

## Project Data Forms Schema
<p align="justify">This is the schema for the project data</p>
![project_data_forms_setup_schema](../img/settings/project_data/project_data_forms_setup_schema.png)

<p align="justify">The project data forms settings has the following tables</p>

<ol>
	<li><a href="#project_data_forms" title="project_data_forms">project_data_forms</a></li>
	<li><a href="#project_data_form_contents" title="project_data_form_contents">project_data_form_contents</a></li>
	<li><a href="#project_data_form_questions" title="project_data_form_questions">project_data_form_questions</a></li>
	<li><a href="#project_data_form_question_item_values" title="project_data_form_question_item_values">project_data_form_question_item_values</a></li>
	<li><a href="#project_data_form_select_options" title="project_data_form_select_options">project_data_form_select_options</a></li>
</ol>

## <a name="project_data_forms"></a><u>project_data_forms</u>
<p align="justify">This table store project data forms informations.</p>

![project_data_forms](../img/settings/project_data/project_data_forms.png)

<a href="#project_data_forms_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#project_data_form_contents" title="Next Table">Next</a>

## <a name="project_data_form_contents"></a><u>project_data_form_contents</u>
<p align="justify">This table store information about the contents of the project data.</p>

![project_data_form_contents](../img/settings/project_data/project_data_form_contents.png)

<a href="#project_data_forms_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#project_data_form_questions" title="Go Top">Next</a>

## <a name="project_data_form_questions"></a><u>project_data_form_questions</u>
<p align="justify">This table store information about the project questions.</p>

![project_data_form_questions](../img/settings/project_data/project_data_form_questions.png)

<a href="#project_data_forms_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#project_data_form_question_item_values" title="Go Top">Next</a>

## <a name="project_data_form_question_item_values"></a><u>project_data_form_question_item_values</u>
<p align="justify">This table store  the item values for the project data questions. It maps information from several tables such as <code>admin_hierarchies, financial_years, project_data_form_questions</code> and <code>projects</code></p>

![project_data_form_question_item_values](../img/settings/project_data/project_data_form_question_item_values.png)

<a href="#project_data_forms_setups" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#project_data_form_select_options" title="Go Top">Next</a>

## <a name="project_data_form_select_options"></a><u>project_data_form_select_options</u>
<p align="justify">This table store the select options available when using projects for planning</p>

![project_data_form_select_options](../img/settings/project_data/project_data_form_select_options.png)

<a href="#project_data_forms_setups" title="Go Top">Back Top</a>