# Setup Module Overview
<p align="justify">This module  deals with all the configurations which are involved in the system. Stores all the settings required for other modules to work. The setup module has several main and sub-main setups that are important and acts as a precondition for the main modules to work. </p>
<p align="justify">The setup module consists of following sub-setps modules </p>
<ol style="text-decoration: underline;">
	<li><a href="http://localhost:8000/settings/admin_hierarchy/">Administrative Settings</a></li>
	<li><a href="http://localhost:8000/usermanagement/user_management/">User Management</a></li>
	<li>Facility</li>
	<li>GFS Code Management</li>
	<li>Finance</li>
	<li>Planning</li>
	<li>Budget</li>
	<li>Financial Year Management</li>
	<li>Project Data Forms</li>
	<li>National References</li>
	<li>Burden of Disease</li>
	<li>Assessment</li>
</ol>