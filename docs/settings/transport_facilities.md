# <a  name="transport_facilities" style="color: unset;">Transport Facilities
<p align="justify">The priority areas guides planners during planning process on what should be the priority areas. After the priority areas has been identified then the planners will continue to set the vehicle_makes that will be applied to address the identified priority areas</p>

## <code>Transport Facilities Schema</code>

![Transport_Facilities_Schema](../img/general_schema/transport_facility_schema.png)

<p align="justify"> The transport facilities has the following tables</p>
<ol>
	<li><a href="#asset_conditions" title="asset_conditions">asset_conditions</a></li>
	<li><a href="#asset_uses" title="asset_uses">asset_uses</a></li>
	<li><a href="#transport_facility_types" title="transport_facility_types">transport_facility_types</a></li>
	<li><a href="#vehicle_makes" title="vehicle_makes">vehicle_makes</a></li>
</ol>

## <a name="asset_conditions"></a><u>asset_conditions</u>
<p align="justify">This table stores pre defined uses of transport assets like Distribution, Supervision, Ambulance etc; for easy management of transport facilities</p>

![asset_conditions](../img/settings/planning_settings/asset_conditions.png)

<a href="#transport_facilities" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#asset_uses" title="Next Table">Next</a>

## <a name="asset_uses"></a><u>asset_uses</u>
<p align="justify">This table store informations about priority areas for planners to look for when doing planning.</p>

![asset_uses](../img/settings/planning_settings/asset_uses.png)

<a href="#transport_facilities" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#transport_facility_types" title="Next Table">Next</a>

## <a name="transport_facility_types"></a><u>transport_facility_types</u>
<p align="justify">This table stores pre defined  transport facility categories;</p>

![transport_facility_types](../img/settings/planning_settings/transport_facility_types.png)

<a href="#transport_facilities" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#vehicle_makes" title="Go Top">Next</a>

## <a name="vehicle_makes"></a><u>vehicle_makes</u>
<p align="justify">This table store information about the vehicle makes  to the vehicles owned by an LGA. It is used in the comprehensive plans to store records related to vehicles and their use.</p>

![vehicle_makes](../img/settings/planning_settings/vehicle_makes.png)

<a href="#transport_facilities" title="Go Top">Back Top</a>