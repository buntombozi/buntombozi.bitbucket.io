# <a  name="national_targets_setups" style="color: unset;">National Targets
<p align="justify">The national targets setting are used by the planning module. All planners must refers to national targets before setting their generic targets and other activities. </p>
<p align="justify"> The national target are recorded in a table called <code><strong><a href="#national_targets" title="national_targets">national_targets</a></strong></code>.</p>

## <a name="national_targets"></a><u>national_targets</u>
<p align="justify">This table store information about the national targets</p>

![national_targets](../img/settings/planning_settings/national_targets.png)

<a href="#national_targets_setups" title="Go Top">Back Top</a>