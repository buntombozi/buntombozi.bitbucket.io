# <a  name="performance_indicators" style="color: unset;">Performance Indicators Settings
<p align="justify">This is a sub-settings module under the planning main setting. The performance indicator is set so as to be used in a  assessment module. When assessing and grading the performance in implementations plan for the given activities/indicators</p>

<p align="justify"> The performance indicator has one table</p>
<ol>
	<li><a href="#performance_indicators" title="performance_indicators">performance_indicators</a></li>
</ol>

## <a name="performance_indicators"></a><u>performance_indicators</u>
<p align="justify">This table store information about the indicators that will be used for assessment on progress of activities, projects etc. it will be used extensively by the assessment module.</p>

![performance_indicators](../img/settings/planning_settings/performance_indicators.png)

<a href="#performance_indicators" title="Go Top">Back Top</a>