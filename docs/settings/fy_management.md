# <a  name="fy_management" style="color: unset;">Financial Year Management
<p align="justify">The financial year management (FYM) settings are prior settings needed by all module. The FYM settings are important for all modules functioning and management as all activities in planrep depends on the given financial year </p>

## Financial Yearm Management Schema
<p align="justify">This is the schema for the financial year managements</p>
![fy_management_schema](../img/settings/fy_management/fy_management_schema.png)

<p align="justify">The FYM settings has the following tables</p>

<ol>
	<li><a href="#financial_years" title="financial_years">financial_years</a></li>
	<li><a href="#period_groups" title="period_groups">period_groups</a></li>
	<li><a href="#periods" title="periods">periods</a></li>
</ol>

## <a name="financial_years"></a><u>financial_years</u>
<p align="justify">This table store information about the financial years that are core to the planning and all other process that will be administered through planrep systems. The financial year are looped into the two words/terms ie <em><strong>open</strong></em> and <em><strong>close</strong></em> of a financial year.</p>

![financial_years](../img/settings/fy_management/financial_years.png)

<a href="#fy_management" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#period_groups" title="Next Table">Next</a>

## <a name="period_groups"></a><u>period_groups</u>
<p align="justify">This table store information about the period groups in a given financial year. The period groups are usefully for several modules such as <em>executions, reporting, assessment , scrutinization</em> etc  Eg.  of periods groups are <em>Annually, monthly, quarterly</em> etc.</p>

![period_groups](../img/settings/fy_management/period_groups.png)

<a href="#fy_management" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#periods" title="Go Top">Next</a>

## <a name="periods"></a><u>periods</u>
<p align="justify">This table store information about the periods that will be used in planrep for a given financial years.</p>

![periods](../img/settings/fy_management/periods.png)

<a href="#fy_management" title="Go Top">Back Top</a>