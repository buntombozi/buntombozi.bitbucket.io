# <a  name="finance_settings" style="color: unset;">Finance Settings
<p align="justify">This is where finance setting are set. The finance involves settings such as the funds, fund types, budget classes etc. This setting are important for the planning, budgeting, scrutinization and other relevant modules of planrep system.</p>

## <code>Finance Settings Schema</code>

![finance_settings](../img/general_schema/finance_settings.png)

<p align="justify">The finance settings has the following tables</p>
<ol>
	<li><a href="#fund_types" title="fund_types">fund_types</a></li>
	<li><a href="#budget_classes" title="budget_classes">budget_classes</a></li>
	<li><a href="#ceilings" title="ceilings">ceilings</a></li>
	<li><a href="#projects" title="projects">projects</a></li>
	<li><a href="#admin_hierarchy_projects" title="admin_hierarchy_projects">admin_hierarchy_projects</a></li>
</ol>

## <a name="fund_types"></a><u>fund_types</u>
<p align="justify">This table stores the Fund types used for classifying fund source categories and later fund sources</p>

![fund_types](../img/settings/finance/fund_types.png)

<a href="#finance_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#budget_classes" title="Next Table">Next</a>

## <a name="budget_classes"></a><u>budget_classes</u>
<p align="justify">This table store informations about different classes of budget such as Development Budget, Recurrent Budget, Recurrent-OC … etc</p>

![budget_classes](../img/settings/finance/budget_classes.png)

<a href="#finance_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#ceilings" title="Go Top">Next</a>

## <a name="ceilings"></a><u>ceilings</u>
<p align="justify">This table store informations about ceilings amounts. It is used mainly by the budgeting/costing module.</p>

![ceilings](../img/settings/finance/ceilings.png)

<a href="#finance_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#ceilings" title="Go Top">Next</a>

## <a name="projects"></a><u>projects</u>
<p align="justify">This table store information about projects that will be undertaken in planrep. The projects will be used in planning, budgeting, execution and other relevance module</p>

![projects](../img/settings/finance/projects.png)

<a href="#finance_settings" title="Go Top">Back Top</a> &nbsp; &nbsp; |&nbsp; &nbsp; <a href="#admin_hierarchy_projects" title="Go Top">Next</a>

## <a name="admin_hierarchy_projects"></a><u>admin_hierarchy_projects</u>
<p align="justify">This table is intended to store informations on projects mapped by admin hierarchy. It mapps information from the admin_hierarchies table and project table.</p>

![admin_hierarchy_projects](../img/settings/finance/admin_hierarchy_projects.png)

<a href="#finance_settings" title="Go Top">Back Top</a> 