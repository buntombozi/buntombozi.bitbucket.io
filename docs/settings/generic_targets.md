# <a  name="generic_targets_setups" style="color: unset;">Generic Targets
<p align="justify">he generic target setting are used by the planning module . The generic targets settings helps the local planners to set targets based on their localities challenges and problems. </p>
<p align="justify"> The generic target  are recorded in a table called  <code><strong><a href="#generic_targets" title="generic_targets">generic_targets</a></strong></code>.</p>

## <a name="generic_targets."></a><u>generic_targets.</u>
<p align="justify">This table store generic target. The generic are mapped with informations from other tables such as <code>plans_chains, sections, priority_areas</code></p>

![generic_targets](../img/settings/planning_settings/generic_targets.png)

<a href="#generic_targets_setups" title="Go Top">Back Top</a>